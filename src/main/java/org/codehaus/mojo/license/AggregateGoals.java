package org.codehaus.mojo.license;


import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.shared.invoker.*;

import java.io.File;
import java.util.Arrays;

@Mojo( name = "run", requiresProject = true, aggregator = true,
        requiresDependencyResolution = ResolutionScope.NONE)
public class AggregateGoals extends AbstractLicenseMojo
{

    // Sections to suppress from the called goals
    static final private String START_REACTOR_ORDER = "[INFO] Reactor Build Order:";
    static final private String END_REACTOR_ORDER = "[INFO] ------------------------------------------------------------------------";

    static final private String START_REACTOR_SUMMARY = "[INFO] Reactor Summary:";
    static final private String END_REACTOR_SUMMARY = "[INFO] ------------------------------------------------------------------------";

    private enum PRINT_STATUS {REACTOR_ORDER, REACTOR_SUMMARY, NORMAL};



    @Override
    protected void init() throws Exception
    {

    }

    @Override
    protected void doAction() throws Exception
    {
        final Invoker invoker = new DefaultInvoker(){

        };
        invoker.setLocalRepositoryDirectory(new File(getSession().getLocalRepository().getBasedir()));

        callGoal(invoker, "generate-bom", "generate BOM file");
        callGoal(invoker, "files-check", "do the license files check");
    }



    private void callGoal(final Invoker invoker, final String goal, final String description) throws MavenInvocationException
    {
        final InvocationRequest request = new DefaultInvocationRequest();
        request.setGoals(Arrays.asList("org.codehaus.mojo:license-maven-plugin:" + goal) );
        request.setShellEnvironmentInherited(true);
        request.setPomFile(getSession().getCurrentProject().getFile());
        request.setProperties(getSession().getUserProperties());
        request.setOutputHandler(new PrintStreamHandler(){



            PRINT_STATUS printStatus = PRINT_STATUS.NORMAL;


            // We want to suppress reactor order and reactor summary from child maven calls
            private void changePrintStatus(String line)
            {

                if ( (printStatus == PRINT_STATUS.REACTOR_ORDER && line.equals(END_REACTOR_ORDER) )
                        || (printStatus == PRINT_STATUS.REACTOR_SUMMARY && line.equals(END_REACTOR_SUMMARY) )
                )
                {
                    printStatus = PRINT_STATUS.NORMAL;
                }

                else
                {
                    if (line.equals(START_REACTOR_ORDER))
                    {
                        printStatus = PRINT_STATUS.REACTOR_ORDER;
                    }
                    else if (line.equals(START_REACTOR_SUMMARY))
                    {
                        printStatus = PRINT_STATUS.REACTOR_SUMMARY;
                    }
                }
            }


            @Override
            public void consumeLine(String s) {

                changePrintStatus(s);


                if (printStatus == PRINT_STATUS.NORMAL)
                {
                    super.consumeLine("\t" + s);
                }
            }
        });


        getLog().debug("Maven properties :" + getSession().getUserProperties().toString());


        getLog().info(">>>>>>>>>>>>>>>>>>>>>>>>>  [ license:"+goal+" - STARTING ]  >>>>>>>>>>>>>>>>>>>>>>>>>");
        final InvocationResult result = invoker.execute( request );

        if ( result.getExitCode() == 0 )
        {
            getLog().info(">>>>>>>>>>>>>>>>>>>>>>>>>  [ license:"+goal+" - FINISHED ]  >>>>>>>>>>>>>>>>>>>>>>>>>");
        }
        else
        {
            throw new IllegalStateException( "Failed to "+ description +". See messages above. " );
        }


    }
}
