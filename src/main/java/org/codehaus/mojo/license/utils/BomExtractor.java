package org.codehaus.mojo.license.utils;


import com.google.common.collect.ImmutableList;
import org.codehaus.mojo.license.model.BomEntry;
import org.codehaus.mojo.license.model.Scope;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public interface BomExtractor
{

    static final String GAV_PATTERN = "([^:]+):([^:]+):([^:]+):([^:]+)";

    /**
     * Retrieve the entries from the bom file, using the filters
     * passed by argument
     * @param bomFile the bom.csv file
     * @param filteredScope the scope (binary/test) desired
     * @param onlyMavenDependencies only maven dependencies
     * @param filteredLicenses include only the licenses in the list
     * @param excludePomDependencies don't include the 'pom' type dependencies
     * @return
     * @throws IOException
     */
    List<BomEntry> retrieveBomEntries(final File bomFile, final Scope filteredScope,
                                      final boolean onlyMavenDependencies,
                                      final ImmutableList<String> filteredLicenses,
                                      final boolean excludePomDependencies ) throws IOException;

    /**
     * Retrieve the entries from the bom file, using the filters
     * passed by argument
     * @param bomFile the bom.csv file
     * @param filteredScope the scope (binary/test) desired
     * @param onlyMavenDependencies only maven dependencies
     * @param excludePomDependencies don't include the 'pom' type dependencies
     * @return
     * @throws IOException
     */
    List<BomEntry> retrieveBomEntries(final File bomFile,final Scope filteredScope,
                                      final boolean onlyMavenDependencies,
                                      final boolean excludePomDependencies) throws IOException;

}
