package org.codehaus.mojo.license.utils;


import com.google.common.collect.ImmutableList;
import org.codehaus.mojo.license.model.BomEntry;
import org.codehaus.mojo.license.model.Scope;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @see {@link BomExtractor}
 * @plexus.component role="org.codehaus.mojo.license.utils.BomExtractor" role-hint="default"
 */
public class DefaultBomExtractor implements BomExtractor
{


    @Override
    public List<BomEntry> retrieveBomEntries(final File thirdPartyFile, final Scope filteredScope,
                                                    final boolean onlyMavenDependencies,
                                                    final ImmutableList<String> filteredLicenses,
                                                    final boolean excludePomDependencies) throws IOException
    {

        final BufferedReader bomFile = new BufferedReader(new FileReader(thirdPartyFile));

        final List<BomEntry> bomEntries = new ArrayList<BomEntry>();


        String line = "";
        while ((line = bomFile.readLine()) != null )
        {
            BomEntry bomEntry = processBomLine(filteredScope, onlyMavenDependencies, filteredLicenses,
                    excludePomDependencies, line);

            if (bomEntry != null)
            {
                bomEntries.add(bomEntry);
            }


        }

        bomFile.close();

        return bomEntries;
    }

    private BomEntry processBomLine(final Scope filteredScope, final boolean onlyMavenDependencies,
                                final ImmutableList<String> filteredLicenses,
                                final boolean excludePomDependencies,
                                final String line) {
        if (line.startsWith("#")) //skip comments
        {
            return null;
        }

        // the BOM comes from the generate-bom goal
        final BomEntry bomEntry = new BomEntry(line);


        if ( bomEntry.getScope().equals("Scope"))   // skip the header
        {
            return null;
        }

        if (filteredScope != null
             && !filteredScope.getPrintableName().equalsIgnoreCase(bomEntry.getScope()) )
        {
            return null;
        }


        if (onlyMavenDependencies && !bomEntry.isMaven())
        {
            return null;
        }

        if (excludePomDependencies && bomEntry.getPackagingType() != null && bomEntry.getPackagingType().equals("pom"))
        {
            return null;
        }


        if (!filteredLicenses.isEmpty() && !containsSubstring(filteredLicenses, bomEntry))
        {
            return null;
        }

        return bomEntry;


    }

    private boolean containsSubstring(ImmutableList<String> filteredLicenses, BomEntry bomEntry) {
        for (String license : filteredLicenses)
        {
            if (bomEntry.getLicense().contains(license))
            {
                return true;
            }
        }

        return false;
    }

    @Override
    public List<BomEntry> retrieveBomEntries(final File thirdPartyFile, final Scope filteredScope,
                                             final boolean onlyMavenDependencies, final boolean excludePomDependencies) throws IOException
    {
        return retrieveBomEntries(thirdPartyFile, filteredScope, onlyMavenDependencies, ImmutableList.<String>of(), excludePomDependencies);
    }
}
