package org.codehaus.mojo.license.utils;

import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.MapMaker;
import com.google.common.collect.Maps;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.manager.WagonManager;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactNotFoundException;
import org.apache.maven.artifact.resolver.ArtifactResolutionException;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.wagon.ConnectionException;
import org.apache.maven.wagon.ResourceDoesNotExistException;
import org.apache.maven.wagon.TransferFailedException;
import org.apache.maven.wagon.UnsupportedProtocolException;
import org.apache.maven.wagon.authentication.AuthenticationException;
import org.apache.maven.wagon.authorization.AuthorizationException;
import org.apache.maven.wagon.providers.http.HttpWagon;
import org.apache.maven.wagon.repository.Repository;
import org.codehaus.mojo.license.api.DownloadArtifactException;
import org.codehaus.plexus.logging.AbstractLogEnabled;
import org.codehaus.plexus.util.xml.Xpp3Dom;
import org.codehaus.plexus.util.xml.Xpp3DomBuilder;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentMap;

/**
 * @see {@link ArtifactCreator}
 * @plexus.component role="org.codehaus.mojo.license.utils.ArtifactCreator" role-hint="default"
 */
public class DefaultArtifactCreator extends AbstractLogEnabled implements ArtifactCreator
{
    private static final List<String> ATLASSIAN_PLUGIN_PACKAGING_TYPES = Collections.unmodifiableList(
            Arrays.asList("atlassian-plugin", "bundle")
    );
    private static final String SHA1_SEARCH_PATH = "/service/local/data_index?sha1=%s";
    private static final Repository SHA1_SEARCH_REPOSITORY = new Repository("central", "https://maven.atlassian.com/");


    private static final String GAV_PATTERN = "([^:]+):([^:]+):([^:]+):([^:]+)";

    /**
     * @plexus.requirement
     */
    private ArtifactFactory artifactFactory;

    /**
     * @plexus.requirement
     */
    private WagonManager wagonManager;

    /**
     * @plexus.requirement
     */
    private PersistentShaCache persistentShaCache;


    /**
     * @plexus.requirement
     */
    private ArtifactResolver resolver;





    // Set in constructor, not injected, purely for testing
    private final ByteArrayOutputStreamCreator outputStreamProvider;

    private final ConcurrentMap<ShaRequest, Supplier<Artifact>> shaCache = new MapMaker().
            makeComputingMap(new Function<ShaRequest, Supplier<Artifact>>()
            {
                @Override
                public Supplier<Artifact> apply(final ShaRequest input)
                {
                    try
                    {
                        final Map<String, Supplier<Artifact>> persistentCache = persistentShaCache.get();

                        if (input.getSha() == null)
                        {
                            return Suppliers.ofInstance(null);
                        }


                        final Supplier<Artifact> artifact = persistentCache.get(input.getSha());
                        if (artifact != null)
                        {
                            return artifact;
                        }
                        else
                        {
                            final String searchResult = writeSearchResultToString(input.getSha());
                            final Artifact artifactFromSearchResult = getArtifactFromSearchResult(searchResult, input.getFileName());
                            return Suppliers.ofInstance(artifactFromSearchResult);
                        }
                    }
                    catch (Exception e)
                    {
                        getLogger().error("An exception occurred when trying to determine the GAV of " + input.getFileName() + " via its SHA", e);
                        return Suppliers.ofInstance(null);
                    }

                }
            });

    private Supplier<HttpWagon> httpWagon = new Supplier<HttpWagon>()
    {
        @Override
        public HttpWagon get()
        {
            try
            {
                return (HttpWagon) wagonManager.getWagon(SHA1_SEARCH_REPOSITORY);
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
        }
    };

    DefaultArtifactCreator()
    {
        this.outputStreamProvider = new DefaultByteArrayOutputStreamCreator();
    }

    DefaultArtifactCreator(ArtifactFactory artifactFactory, WagonManager wagonManager, ByteArrayOutputStreamCreator outputStreamProvider,
                           PersistentShaCache persistentShaCache)
    {
        this.artifactFactory = artifactFactory;
        this.wagonManager = wagonManager;
        this.outputStreamProvider = outputStreamProvider;
        this.persistentShaCache = persistentShaCache;
    }

    public Artifact fromPom(final InputStream fileInputStream, final String fileName)
    {
        try
        {
            /*Xpp3Dom build = Xpp3DomBuilder.build(new BufferedReader(new InputStreamReader(fileInputStream)));
            Xpp3Dom possibleArtifactId = build.getChild("artifactId");
            Xpp3Dom possibleGroupId = build.getChild("groupId");
            if (possibleGroupId == null) {
                possibleGroupId = build.getChild("parent").getChild("groupId");
            }
            Xpp3Dom possibleVersion = build.getChild("version");
            if (possibleVersion == null) {
                possibleVersion = build.getChild("parent").getChild("version");
            }
            Xpp3Dom possiblePackaging = build.getChild("packaging");
            String possiblePackagingString =  (possiblePackaging == null || ATLASSIAN_PLUGIN_PACKAGING_TYPES.contains(possiblePackaging.getValue())) ?
                    "jar" : possiblePackaging.getValue();

            return artifactFactory.createArtifact(possibleGroupId.getValue(), possibleArtifactId.getValue(),
                    possibleVersion.getValue(), null, possiblePackagingString);*/

            Properties pomProperties = new Properties();
            pomProperties.load(new BufferedInputStream(fileInputStream));

            String groupId = (String)pomProperties.get("groupId");
            String artifactId = (String)pomProperties.get("artifactId");
            String version = (String)pomProperties.get("version");
            String packaging = "jar";


            Artifact artifact = fromGAV(groupId, artifactId, version, packaging);

            artifact.setScope("compile");

            return artifact;
        }
        catch (Exception e)
        {
            getLogger().error("An exception occurred when trying to determine the GAV of " + fileName + " via its pom", e);
            return null;
        }
    }

    @Override
    public Artifact fromGAV(String groupId, String artifactId, String version, String packaging) {
        Artifact artifact = artifactFactory.createArtifact(groupId, artifactId,
                version, null, packaging);

        return artifact;
    }


    @Override
    public Artifact fromGAV(String groupId, String artifactId, String version, String packaging, String classifier) {

        return artifactFactory.createArtifactWithClassifier(groupId, artifactId, version, packaging, classifier);
    }


    @Override
    public void resolveArtifact(Artifact artifact, List remoteRepositories, ArtifactRepository artifactRepository) throws ArtifactNotFoundException, ArtifactResolutionException {
        resolver.resolveAlways(artifact, remoteRepositories, artifactRepository);
    }

    @Override
    public Artifact resolveArtifact(final String groupId, final String artifactId, final String version,
                                    final String classifier, final String packaging,
                                    final List remoteRepositories, final ArtifactRepository artifactRepository) throws DownloadArtifactException {

        final Artifact  mavenArtifact = fromGAV(groupId, artifactId,
                version, packaging, classifier);

        try {
            resolveArtifact(mavenArtifact, remoteRepositories, artifactRepository );
        } catch (ArtifactNotFoundException e) {
            throw new DownloadArtifactException("Error retrieving artifact from maven", e);
        } catch (ArtifactResolutionException e) {
            throw new DownloadArtifactException("Error retrieving artifact from maven", e);
        }

        return mavenArtifact;

    }

    @Override
    public boolean checkArtifact(final String groupId, final String artifactId, final String version,
                                             final String classifier, final String packaging,
                                             final List remoteRepositories, final ArtifactRepository artifactRepository) throws DownloadArtifactException {

        /*
         Looks like the maven API version 2.2.1 doesn't offer a way
         of testing if an artifact exist without downloading it.
         TODO: we can change it after upgrading to API 3.0.5
         http://stackoverflow.com/a/4809634
         */


         return resolveArtifact(groupId, artifactId, version, classifier, packaging, remoteRepositories, artifactRepository)  != null;

    }


    public Artifact fromSha(final String sha, final String fileName)
    {
        return shaCache.get(new ShaRequest(sha, fileName)).get();
    }


    @Override
    public void persistShaCache()
    {
        final Map<String, Artifact> cache = Maps.newHashMap();
        for (Map.Entry<ShaRequest, Supplier<Artifact>> shaRequestSupplierEntry : shaCache.entrySet())
        {
            cache.put(shaRequestSupplierEntry.getKey().getSha(), shaRequestSupplierEntry.getValue().get());
        }
        persistentShaCache.write(cache);
    }

    private Artifact getArtifactFromSearchResult(final String searchResult, String fileName)
            throws IOException, XmlPullParserException, ArtifactResolutionException, ArtifactNotFoundException
    {
        Xpp3Dom build = Xpp3DomBuilder.build(new BufferedReader(new StringReader(searchResult)));
        Xpp3Dom totalCount = build.getChild("totalCount");
        if (totalCount != null && Integer.parseInt(totalCount.getValue()) > 0 &&
                build.getChild("data").getChildCount() > 0)
        {
            Xpp3Dom artifact = build.getChild("data").getChild(0);
            String groupId = artifact.getChild("groupId").getValue();
            String artifactId = artifact.getChild("artifactId").getValue();
            String version = artifact.getChild("version").getValue();
            Xpp3Dom classifier = artifact.getChild("classifier");
            String classifierString = null;
            if (classifier != null)
            {
                classifierString = classifier.getValue();
            }
            String[] split = fileName.split("\\.");
            String extension = split.length > 0 ? split[split.length - 1] : null;

            Artifact artifactWithClassifier = artifactFactory.createArtifactWithClassifier(groupId, artifactId, version, extension, classifierString);

            artifactWithClassifier.setScope("compile");

            return artifactWithClassifier;
        }
        else
        {
            return null;
        }
    }




    private String writeSearchResultToString(String sha) throws UnsupportedProtocolException, ConnectionException,
            AuthenticationException, TransferFailedException, ResourceDoesNotExistException,
            AuthorizationException, IOException
    {
        final ByteArrayOutputStream baos = outputStreamProvider.create();
        final HttpWagon streamingWagon = httpWagon.get();
        streamingWagon.connect(SHA1_SEARCH_REPOSITORY);
        streamingWagon.getToStream(String.format(SHA1_SEARCH_PATH, sha), baos);
        return baos.toString();
    }

    interface ByteArrayOutputStreamCreator
    {
        public ByteArrayOutputStream create() throws IOException;
    }

    class DefaultByteArrayOutputStreamCreator implements ByteArrayOutputStreamCreator
    {
        public ByteArrayOutputStream create() throws IOException
        {
            return new ByteArrayOutputStream();
        }
    }

    private static class ShaRequest
    {
        private final String sha;
        private final String fileName;

        public ShaRequest(final String sha, final String fileName)
        {
            this.sha = sha;
            this.fileName = fileName;
        }

        public String getSha()
        {
            return sha;
        }

        public String getFileName()
        {
            return fileName;
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ShaRequest that = (ShaRequest) o;

            if (fileName != null ? !fileName.equals(that.fileName) : that.fileName != null) return false;
            if (sha != null ? !sha.equals(that.sha) : that.sha != null) return false;

            return true;
        }

        @Override
        public int hashCode()
        {
            int result = sha != null ? sha.hashCode() : 0;
            result = 31 * result + (fileName != null ? fileName.hashCode() : 0);
            return result;
        }
    }

}
