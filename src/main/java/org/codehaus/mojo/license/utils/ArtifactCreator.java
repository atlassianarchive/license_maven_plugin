package org.codehaus.mojo.license.utils;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactNotFoundException;
import org.apache.maven.artifact.resolver.ArtifactResolutionException;
import org.apache.maven.plugin.logging.Log;
import org.codehaus.mojo.license.api.DownloadArtifactException;
import org.codehaus.mojo.license.model.BomEntry;

import java.io.File;
import java.io.InputStream;
import java.util.List;

/**
 * Creates {@link Artifact}s based on some hint, such as a pom.xml or its SHA-1 hash. Used to determine the
 * actual coordinates of some arbitrarily supplied archive.
 */
public interface ArtifactCreator
{
    /**
     * Creates an {@link Artifact} based on a pom, expected to be supplied via an {@link InputStream}
     *
     * @param inputStream the inputStream created from a pom.xml
     * @param fileName the fileName of the enclosing pom.xml artifact, provided for debugging purposes
     * @return An @{@link Artifact} based on coordinates inferred from the supplied pom
     */
    Artifact fromPom(InputStream inputStream, String fileName);

    /**
     * Creates an {@link Artifact} based on the SHA-1 hash of a file
     *
     * @param sha the SHA-1 hash of the artifact
     * @param fileName the fileName of the SHA-1 hash-ed artifact, provided for debugging purposes
     * @return An @{@link Artifact} based on a search done
     */
    Artifact fromSha(String sha, String fileName);


    /**
     * Creates an {@link Artifact} based on a GAV
     */
    Artifact fromGAV(String groupId, String artifactId, String version, String packaging);

    /**
     * Creates an {@link Artifact} based on a GAV
     */
    Artifact fromGAV(String groupId, String artifactId, String version, String packaging, String classifier);


    void resolveArtifact(Artifact artifact, List remoteRepositories, ArtifactRepository artifactRepository) throws ArtifactNotFoundException, ArtifactResolutionException;


    /**
     * Based on the GAV, create and resolve it.
     *
     * @param groupId
     * @param artifactId
     * @param version
     * @param classifier
     * @param packaging
     * @param remoteRepositories
     * @param artifactRepository
     * @return
     * @throws DownloadArtifactException
     */
    Artifact resolveArtifact(String groupId, String artifactId, String version,
                             String classifier, String packaging,
                             List remoteRepositories, ArtifactRepository artifactRepository)
            throws DownloadArtifactException;


    /**
     * Check if the artifact exist
     *
     * @param groupId
     * @param artifactId
     * @param version
     * @param classifier
     * @param packaging
     * @param remoteRepositories
     * @param artifactRepository
     * @return
     */
    boolean checkArtifact(final String groupId, final String artifactId, final String version,
                                 final String classifier, final String packaging,
                                 final List remoteRepositories, final ArtifactRepository artifactRepository) throws DownloadArtifactException;


    void persistShaCache();

}
