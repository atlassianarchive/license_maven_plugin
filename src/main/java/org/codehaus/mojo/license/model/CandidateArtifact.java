package org.codehaus.mojo.license.model;

import org.apache.maven.artifact.Artifact;

import java.util.ArrayList;
import java.util.List;

/**
 * Model for {@link org.codehaus.mojo.license.api.DefaultAtlassianPluginHelper}
 *
 * @since 1.3-atlassian-1.4
 */
public class CandidateArtifact
{
    /**
     * SHA-1 hash of the artifact we are resolving
     */
    private final String sha;
    private final String fileName;
    private final String fullPath;

    /**
     * Artifacts may contain pom.xml files generated if they are built using maven. When trying to identify
     * an archive, if the SHA-1 search is unsuccessful, this list is used instead.
     */
    private final List<Artifact> containedArtifacts = new ArrayList<Artifact>();

    public CandidateArtifact(String sha, String fileName, String fullPath)
    {
        this.sha = sha;
        this.fileName = fileName;
        this.fullPath = fullPath;
    }

    public void addContainedArtifact(final Artifact artifact)
    {
        containedArtifacts.add(artifact);
    }

    public String getSha()
    {
        return sha;
    }

    public String getFileName()
    {
        return fileName;
    }

    public String getFullPath()
    {
        return fullPath;
    }

    public List<Artifact> getContainedArtifacts()
    {
        return containedArtifacts;
    }

    @Override
    public String toString() {
        return "CandidateArtifact{" +
                "sha='" + sha + '\'' +
                ", fileName='" + fileName + '\'' +
                ", fullPath='" + fullPath + '\'' +
                ", containedArtifacts=" + containedArtifacts +
                '}';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CandidateArtifact that = (CandidateArtifact) o;

        if (containedArtifacts != null ? !containedArtifacts.equals(that.containedArtifacts) : that.containedArtifacts != null)
            return false;
        if (fileName != null ? !fileName.equals(that.fileName) : that.fileName != null) return false;
        if (fullPath != null ? !fullPath.equals(that.fullPath) : that.fullPath != null) return false;
        if (sha != null ? !sha.equals(that.sha) : that.sha != null) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = sha != null ? sha.hashCode() : 0;
        result = 31 * result + (fileName != null ? fileName.hashCode() : 0);
        result = 31 * result + (fullPath != null ? fullPath.hashCode() : 0);
        result = 31 * result + (containedArtifacts != null ? containedArtifacts.hashCode() : 0);
        return result;
    }
}
