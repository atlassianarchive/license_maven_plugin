package org.codehaus.mojo.license.model;


import org.apache.maven.project.MavenProject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Dependency implements Comparable
{

    abstract public String getArtifactName();

    abstract public String getName();

    abstract public String getId();

    abstract public String getUrl();

    abstract public String getArtifactScope();


    public abstract List<License> getLicenses();

    public abstract boolean isComplete();

    public abstract String getGAVInfo();

    public Map<String, Scope> scopesPerModule = new HashMap<String, Scope>();


    public boolean hasLowerEqualsScope(Scope scope)
    {
        return !(this.getLicenseScope() == Scope.BINARY
                && scope == Scope.TEST);
    }

    public Scope getLicenseScope()
    {
        for (Scope scope : scopesPerModule.values())
        {
            if (scope == Scope.BINARY)
            {
                return Scope.BINARY;
            }
        }

        return Scope.TEST;
    }


    public int compareTo(Object o)
    {
       Dependency dependency = (Dependency) o;
       return getArtifactName().compareTo(dependency.getArtifactName());
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null) return false;

        Dependency that = (Dependency) o;

        if (getName() != null ? !getName().equals(that.getName()) : that.getName() != null) return false;
        if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = getName() != null ? getName().hashCode() : 0;
        result = 31 * result + (getId() != null ? getId().hashCode() : 0);
        return result;
    }


    public void addScopeOnModule(MavenProject mavenProject, Scope scope)
    {
        String id = getProjectId(mavenProject);
        this.scopesPerModule.put(id, scope);
    }


    public Map<String, Scope> getScopesPerModule()
    {
        return scopesPerModule;
    }

    public boolean containsModule(MavenProject mavenProject)
    {
        String id = getProjectId(mavenProject);
        return scopesPerModule.containsKey(id);
    }

    private String getProjectId(MavenProject mavenProject)
    {
        return mavenProject.getArtifact().getId();
    }

    public void addScopeOnModule(Map<String, Scope> scopesPerModule)
    {
        scopesPerModule.putAll(scopesPerModule);
    }
}
