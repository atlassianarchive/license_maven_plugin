package org.codehaus.mojo.license.model;

/*
 * #%L
 * License Maven Plugin
 * %%
 * Copyright (C) 2008 - 2011 CodeLutin, Codehaus, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


import com.google.common.collect.Ordering;
import com.google.common.collect.SortedSetMultimap;
import com.google.common.collect.TreeMultimap;
import org.apache.maven.project.MavenProject;
import org.codehaus.mojo.license.utils.MojoHelper;


import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Map of artifacts (stub in mavenproject) group by their license.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class LicenseMap
{

    private static final long serialVersionUID = 864199843545688069L;


    public static final String UNKNOWN_LICENSE_MESSAGE = "Unknown license";

    private final Comparator<MavenProject> mavenProjectComparator;

    //private Map<String, SortedSet<Dependency>> licenseMap = new TreeMap<String, SortedSet<Dependency>>();
    private SortedSetMultimap<String, Dependency> licenseMap = TreeMultimap.create();

    /**
     * Default contructor.
     */
    public LicenseMap()
    {
        mavenProjectComparator = MojoHelper.newMavenProjectComparator();
    }


    private <T extends Dependency> SortedSetMultimap<String, T> getSubclassDependenciesMap(Class<T> tClass)
    {
        SortedSetMultimap<String, T> dependencies = TreeMultimap.create();

        for (Map.Entry<String, Dependency> entry : licenseMap.entries())
        {
            if (tClass.isAssignableFrom(entry.getValue().getClass()))
            {
                dependencies.put(entry.getKey(), (T) entry.getValue());
            }
        }
        return dependencies;
    }


    private <T extends Dependency> SortedSet<T> getSubclassDependenciesSet(Class<T> tClass, String license)
    {

        Set<Dependency> dependencies = null ;

        if (license != null)
        {
            dependencies = licenseMap.get(license);
        }
        else
        {
            dependencies = new HashSet<Dependency>(licenseMap.values());
        }

        SortedSet<T> dependenciesByClass = new TreeSet<T>();

        for (Dependency dependency : dependencies)
        {
            if (tClass.isAssignableFrom(dependency.getClass()))
            {
                dependenciesByClass.add((T)dependency);
            }
        }
        return dependenciesByClass;
    }


    public Set<Map.Entry<String,Dependency>> entries()
    {
        return licenseMap.entries();
    }

    public  SortedSet<MavenProject> getMavenProjects()
    {
       TreeSet<MavenProject> mavenProjects = new TreeSet<MavenProject>(mavenProjectComparator);
       Set<MavenDependency> dependencies = getSubclassDependenciesSet(MavenDependency.class, null);

       for (MavenDependency mavenDependency : dependencies)
       {
           mavenProjects.add(mavenDependency.getMavenProject());
       }

       return mavenProjects;

    }

    public Map<String, Collection<MavenProject>> getMavenProjectsPerLicense()
    {
        SortedSetMultimap<String, MavenProject> mavenProjectsMap = TreeMultimap.create(Ordering.natural(), mavenProjectComparator);
        SortedSetMultimap<String, MavenDependency> dependencies = getSubclassDependenciesMap(MavenDependency.class);

        for (Map.Entry<String, MavenDependency> entry : dependencies.entries())
        {
            mavenProjectsMap.put(entry.getKey(), entry.getValue().getMavenProject() );
        }

        return mavenProjectsMap.asMap();
    }




    public SortedMap<MavenProject, String[]> getLicensesPerMavenProject()
    {

        SortedSetMultimap<MavenProject, String> mavenProjectsLicenses = TreeMultimap.create(mavenProjectComparator, Ordering.natural());

        SortedSetMultimap<String, MavenDependency> mavenDependencies = getSubclassDependenciesMap(MavenDependency.class);
        for (Map.Entry<String, MavenDependency> entry : mavenDependencies.entries())
        {
            mavenProjectsLicenses.put(entry.getValue().getMavenProject(), entry.getKey());
        }

        SortedMap<MavenProject, String[]> licensesPerProject = new TreeMap<MavenProject, String[]>(mavenProjectComparator);
        for (MavenProject mavenProject : mavenProjectsLicenses.keySet())
        {
            licensesPerProject.put(mavenProject,mavenProjectsLicenses.get(mavenProject).toArray(new String[0]));
        }


        return licensesPerProject;

    }




    /**
     * Build a dependencies map from the license map, this is a map of license for each project registred in the
     * license map.
     *
     * @return the generated dependencies map
     */
    public SortedMap<Dependency, String[]> getLicensesPerDependency()
    {

        SortedSetMultimap<Dependency, String> projectsLicenses = TreeMultimap.create();

        SortedSetMultimap<String, Dependency> dependencies = getSubclassDependenciesMap(Dependency.class);
        for (Map.Entry<String, Dependency> entry : dependencies.entries())
        {
            projectsLicenses.put(entry.getValue(), entry.getKey());
        }

        SortedMap<Dependency, String[]> licensesPerProject = new TreeMap<Dependency, String[]>();
        for (Dependency dependency : projectsLicenses.keySet())
        {
            licensesPerProject.put(dependency,projectsLicenses.get(dependency).toArray(new String[0]));
        }
        return licensesPerProject;
    }


    public SortedSet<MavenProject> getMavenProjectsByLicense(String license)
    {
        SortedSet<MavenDependency> dependencies = getSubclassDependenciesSet(MavenDependency.class, license);


        SortedSet<MavenProject> mavenDeps = new TreeSet<MavenProject>(mavenProjectComparator);

        for (MavenDependency dependency : dependencies)
        {
             mavenDeps.add(dependency.getMavenProject());
        }


        return mavenDeps;
    }


    public Set<String> getAllLicenses()
    {
        return licenseMap.keySet();
    }

    public SortedSet<Dependency> getDependenciesByLicense(String license)
    {
        return licenseMap.get(license);
    }


    /**
     * Store in the license map a project to a given license.
     *
     * @param license   the license on which to associate the gieven project
     * @param value project to register in the license map
     * @return the set of projects using the given license
     */
    public void addDependency(String license, Dependency value)
    {
        licenseMap.put( license, value );
    }

    public void addDependency(String license, MavenProject value, Scope scope, MavenProject parentProject)
    {
        addDependency(license, new MavenDependency(parentProject, value, scope));

    }
    public void addDependencies(String license, SortedSet<Dependency> value)
    {
        for (Dependency dependency : value)
        {
            licenseMap.put(license, dependency);
        }
    }

    public void removeLicense (String license)
    {
        licenseMap.removeAll(license);
    }

    public void overrideLicenseAndScope(Dependency value, String license)
    {
        Set<String> licenses = new HashSet<String>(licenseMap.keySet());

        for (String key : licenses)
        {
            licenseMap.remove(key, value);
        }

        licenseMap.put(license, value);
    }

    public int numberOfLicenses()
    {
        return licenseMap.size();
    }

    public boolean containsDependency(Dependency value)
    {
        return licenseMap.containsValue(value);
    }

    public Dependency getValue(Dependency value)
    {
        for (Dependency dependency : licenseMap.values())
        {
            if (dependency.equals(value))
            {
                return dependency;
            }
        }

        return null;
    }


    public void removeOtherLicensesOfDependency(Dependency dependency, String license)
    {
        Set<String> licenses = new HashSet<String>(licenseMap.keySet());

        for (String key : licenses)
        {
            if (license == null || !key.equals(license))
            {
                licenseMap.remove(key, dependency);
            }
        }
    }

    public void removeLicenses(List<String> ignoredLicenses)
    {
        for (String license : ignoredLicenses)
        {
            removeLicense(license);
        }
    }

    public Collection<Dependency> getProjects()
    {
        return licenseMap.values();
    }
}
