package org.codehaus.mojo.license.model;


import org.codehaus.mojo.license.utils.BomExtractor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BomEntry
{

    final private String componentName;
    final private String group;
    final private String artifactId;
    final private String version;
    final private String packagingType;


    final private String license;
    final private String scope;
    final private boolean isMaven;


    public String getComponentName() {
        return componentName;
    }

    public String getLicense() {
        return license;
    }

    public String getScope() {
        return scope;
    }

    public String getGroup() {
        return group;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public String getVersion() {
        return version;
    }

    public String getPackagingType() {
        return packagingType;
    }

    public boolean isMaven() {
        return isMaven;
    }

    public String getComposedIdentifier()
    {
        if (isMaven)
        {
           return group + ":" + artifactId + ":" + packagingType + ":" + version;
        }
        else
        {
            return componentName + ":" + version;
        }

    }

    public String toString()
    {
        return getComposedIdentifier() +  " (" + getLicense() + ")";
    }



    public BomEntry(String BOMline)
    {
        final String[] columns = BOMline.split(",");

        componentName = columns[0];

        license = columns[2];
        scope = columns[4];



        final String groupArtifactidVersion = columns[1];

        Matcher gavMatcher = Pattern.compile(BomExtractor.GAV_PATTERN).matcher(groupArtifactidVersion);

        if (!gavMatcher.matches())   // non-maven artifact
        {
            isMaven = false;
            version = groupArtifactidVersion;
            group = null;
            artifactId = null;
            packagingType = null;
        }
        else
        {
            isMaven = true;
            group = gavMatcher.group(1);
            artifactId = gavMatcher.group(2);
            packagingType = gavMatcher.group(3);
            version = gavMatcher.group(4);
        }



    }
}
