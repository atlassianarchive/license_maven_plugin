package org.codehaus.mojo.license;


import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.mojo.license.utils.ArtifactCreator;
import org.codehaus.mojo.license.utils.BomExtractor;

import java.io.File;
import java.util.List;

public abstract class AbstractBomExtractorMojo extends AbstractLicenseMojo
{
    @Component( role=ArtifactCreator.class)
    protected ArtifactCreator artifactCreator;

    @Component(role = BomExtractor.class)
    protected BomExtractor bomExtractor;



    /**  For some reason, I can't get these two inside ArtifactCreator
     *   Probably because it's using the old-fashion component annotation.
     *   and I don't know how to create a new component using the new way **/
    @Parameter(defaultValue = "${localRepository}" )
    protected ArtifactRepository artifactRepository;


    @Parameter(defaultValue = "${project.remoteArtifactRepositories}")
    protected List remoteRepositories;


    protected File thirdPartyFile;


    @Override
    protected void init() throws Exception
    {

        thirdPartyFile = new File(getOutputDirectory(), getThirdPartyFilename());

        if (!thirdPartyFile.exists() || !thirdPartyFile.isFile())
        {
            throw new IllegalArgumentException("The BOM file "+ thirdPartyFile.getAbsolutePath() + " doesn't exist or it's not a file. ");
        }

    }
}
