package org.codehaus.mojo.license;

/*
 * #%L
 * License Maven Plugin
 * %%
 * Copyright (C) 2008 - 2011 CodeLutin, Codehaus, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.codehaus.mojo.license.api.AtlassianPluginHelper;

import java.util.List;
import java.util.Set;

/**
 * This is an attempt to create an add-third-party goal that works nicely with atlassian-plugins packaging
 */
@Mojo( name = "atlassian-plugin", requiresProject = true, requiresDependencyResolution = ResolutionScope.TEST)
public class AtlassianPluginAddThirdPartyMojo
    extends AbstractLicenseMojo
{

    @Component
    private AtlassianPluginHelper atlassianPluginHelper;

    @Parameter(defaultValue = "${localRepository}")
    protected ArtifactRepository artifactRepository;

    @Parameter (defaultValue="${project.remoteArtifactRepositories}")
    private List remoteRepositories;

    @Override
    protected void init() throws Exception
    {
    }

    @Override
    protected void doAction() throws Exception
    {
    }

    public void execute()
    {

        try {
            final Set<Artifact> artifacts = atlassianPluginHelper.resolveNestedArtifacts(getProject().getArtifacts(),
                    remoteRepositories, artifactRepository, false);
            for (Artifact a : artifacts) {
                getLog().info("> final plugin artifact " + a.toString());
            }

        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
