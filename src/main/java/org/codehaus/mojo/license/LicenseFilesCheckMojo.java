package org.codehaus.mojo.license;


import de.schlichtherle.truezip.file.TFile;
import de.schlichtherle.truezip.file.TFileInputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.codehaus.mojo.license.model.BomEntry;
import org.codehaus.mojo.license.model.Scope;
import org.codehaus.plexus.util.IOUtil;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/*
 * This goal receives a BOM and assumes it's correct.
 */
@Mojo( name = "files-check", requiresProject = false, aggregator = true,
        requiresDependencyResolution = ResolutionScope.NONE)
public class LicenseFilesCheckMojo extends AbstractBomExtractorMojo
{

    private static final String SEPARATOR = "--";
    private static final String FILEPATH_FORBIDDEN_CHARS = "[^A-Za-z0-9.-]";



    @Parameter( property = "license.licensesDirectory", defaultValue = "licenses", required = true )
    private File licensesDirectory;



    // Map<filename, size_of_file>
    private Map<String, Long> unreviewedLicenseFiles;


    // Map<filename, BOMEntry>
    private Map<String, BomEntry> bomInfo;


    //Set<filename>
    final Set<String> missingFiles = new HashSet<String>();
    final Set<String> emptyFiles = new HashSet<String>();



    @Override
    protected void init() throws Exception
    {

        super.init();


        if (!licensesDirectory.exists() || !licensesDirectory.isDirectory())
        {
            throw new IllegalArgumentException("The license directory "+ licensesDirectory.getAbsolutePath() + " doesn't exist");
        }

        // files finishing with .txt on the license folder
        final File[] files = licensesDirectory.listFiles(new FileFilter()
        {
            public boolean accept(File pathname)
            {
                return pathname.isFile()
                        && pathname.getName().toLowerCase().endsWith(".txt");
            }
        });

        unreviewedLicenseFiles = new HashMap<String, Long>();
        for (File file : files)
        {
            unreviewedLicenseFiles.put(file.getName(), file.length());
        }


    }

    @Override
    protected void doAction() throws Exception
    {

        final List<BomEntry> bomEntries = bomExtractor.retrieveBomEntries(thirdPartyFile, Scope.BINARY, false, true);

        bomInfo =  processBOMEntries(bomEntries);

        if (hasErrors())
        {
            processErrors();
        }

        if (!unreviewedLicenseFiles.isEmpty())
        {
            processUnusedFiles();
        }

        if (hasErrors())
        {
            throw new FileNotFoundException("Some license files are missing. See message above for details. ");
        }

    }

    private void processUnusedFiles() {
        final StringBuilder unusedFilesMsg = new StringBuilder("\n\nThe following files are not necessary:\n");

        for (String filePath : unreviewedLicenseFiles.keySet())
        {
            unusedFilesMsg.append(" - ").append(filePath).append("\n");
        }
        unusedFilesMsg.append("\n\n");

        getLog().warn(unusedFilesMsg);
    }

    private boolean hasErrors() {
        return !missingFiles.isEmpty() || !emptyFiles.isEmpty();
    }

    private void processErrors() throws Exception {
        final StringBuilder errorMsg = new StringBuilder("\n\nThe following files are missing or empty in the license folder:\n");

        for (String fileName : missingFiles)
        {
            final BomEntry bomEntry = bomInfo.get(fileName);

            errorMsg.append(String.format(" - %s \n\t ( %s | %s | %s )\n", fileName,
                    bomEntry.getComponentName(), bomEntry.getComponentName(), bomEntry.getLicense() ));

            //Try to give the user some hints of files to rename/update
            final String filePrefix = retrievePrefixFromFileName(fileName);

            final List<String> possibleUpgrades = getPossibleFilesToUpgrade(filePrefix);

            doActionsForMissingFiles(errorMsg, fileName, possibleUpgrades);

            retrieveLicenseFileContent(errorMsg, fileName, bomEntry);

        }

        for (String fileName : emptyFiles)
        {

            final BomEntry bomEntry = bomInfo.get(fileName);
            errorMsg.append(String.format(" - %s \n\t ( %s | %s | %s )\n", fileName,
                    bomEntry.getComponentName(), bomEntry.getComponentName(), bomEntry.getLicense() ));

            errorMsg.append("\t Fill the content of ").append(fileName).append(". \n");

            retrieveLicenseFileContent(errorMsg, fileName, bomEntry);
        }

        errorMsg.append("\nMake sure you review the content of all files above.");

        errorMsg.append("\n\n");
        getLog().error(errorMsg);


    }

    private void retrieveLicenseFileContent(final StringBuilder errorMsg, final String affectedFile, final BomEntry bomEntry) throws Exception
    {
        final File licenseFile = new File(licensesDirectory, affectedFile);

        if (!licenseFile.exists())
        {
            getLog().debug("File " + affectedFile + " doesn't exist. ");
            return;
        }

        if (!bomEntry.isMaven())
        {
            errorMsg.append("\t Not possible to retrieve the license files of non maven artifacts. \n");
            return;
        }

        final Artifact artifact = artifactCreator.fromGAV(bomEntry.getGroup(), bomEntry.getArtifactId(),
                bomEntry.getVersion(), bomEntry.getPackagingType());

        try
        {
            artifactCreator.resolveArtifact(artifact, remoteRepositories, artifactRepository);

        } catch (Exception e)
        {
            errorMsg.append("\t Not possible to retrieve artifact from maven.\n");
            return;
        }


        final List<TFile> filesInArchive = getOrderedLicenseFiles(artifact);

        if (filesInArchive.size() == 0)
        {
            errorMsg.append("\t No licenses files available.\n");
            return;
        }

        errorMsg.append("\t Retrieving the content from: ");
        errorMsg.append(StringUtils.join(getRelativeLicenseFilesNames(artifact, filesInArchive), ",")).append("\n");

        final FileOutputStream fileWriter = new FileOutputStream(licenseFile);

        for (TFile tFile : filesInArchive)
        {
            retrieveSingleFileContent(fileWriter, tFile);
        }

        fileWriter.close();

    }

    private List<TFile> getOrderedLicenseFiles(Artifact artifact) {
        final List<TFile> filesInArchive = findLicenseFiles(new TFile(artifact.getFile()));

        Collections.sort(filesInArchive, new Comparator<TFile>() {
            @Override
            public int compare(TFile o1, TFile o2) {
                return o2.getName().compareTo(o1.getName());
            }
        });
        return filesInArchive;
    }

    private List<String> getRelativeLicenseFilesNames(Artifact artifact, List<TFile> filesInArchive) {
        final List<String> relativeLicenseFiles = new ArrayList<String>();
        for (File file : filesInArchive)
        {
             relativeLicenseFiles.add(file.getAbsolutePath().replace(artifact.getFile().getAbsolutePath() + File.separator, ""));
        }
        return relativeLicenseFiles;
    }

    private void retrieveSingleFileContent(FileOutputStream fileWriter, TFile tFile) {
        TFileInputStream tFileInputStream = null;


        try
        {
            tFileInputStream = new TFileInputStream(tFile.toNonArchiveFile());

            IOUtils.copyLarge(tFileInputStream, fileWriter);
            fileWriter.write('\n');
            fileWriter.write('\n');


        }
        catch (Exception e)
        {
            getLog().error("An error occurred when trying to extract licenses files from " + tFile.getName(), e);
        }
        finally
        {
            IOUtil.close(tFileInputStream);
        }
    }

    private List<TFile> findLicenseFiles(final TFile parent) {
        final List<TFile> foundFiles = new ArrayList<TFile>();

        if (parent.listFiles() == null)
            return foundFiles;

        for (TFile child : parent.listFiles())
        {
            if (child.isDirectory())
            {
                foundFiles.addAll(findLicenseFiles(child));
            }

            else if (child.getName().matches("(?i)licen[cs]e(?:\\.txt)?|notice(?:\\.txt)?"))
            {
                foundFiles.add(child);
            }

        }

        return foundFiles;
    }

    private Map<String, BomEntry> processBOMEntries(final List<BomEntry> bomEntries) {

        final Map<String, BomEntry> bomInfo = new HashMap<String, BomEntry>();


        for (BomEntry bomEntry : bomEntries)
        {
            final String fileName = retrieveFilename(bomEntry.getComponentName(), bomEntry);

            bomInfo.put(fileName, bomEntry);


            if (!unreviewedLicenseFiles.keySet().contains(fileName))
            {
                missingFiles.add(fileName);
                getLog().debug("Missing license file: " + fileName);
                continue;

            }

            if (unreviewedLicenseFiles.get(fileName) == 0)
            {
                emptyFiles.add(fileName);
                getLog().debug("Empty license file: " + fileName);
            }

            unreviewedLicenseFiles.remove(fileName);
            getLog().debug("Existing license file: " + fileName);
        }

        return bomInfo;

    }

    private void doActionsForMissingFiles(final StringBuilder errorMsg, final String fileName,
                                          final List<String> possibleUpgrades) throws IOException {
        if (possibleUpgrades.isEmpty())
        {
            errorMsg.append("\t Creating a file for it. Make sure its content is correct before commiting.\n");
            new File(licensesDirectory, fileName).createNewFile();
            return;
        }

        if (possibleUpgrades.size() == 1)
        {
            final String possibleFileToUpgrade = possibleUpgrades.get(0);


            errorMsg.append("\t Renaming the file ").append(possibleFileToUpgrade).append(". Check if it has the right content before commiting.\n");
            FileUtils.moveFile(new File(licensesDirectory, possibleFileToUpgrade), new File(licensesDirectory, fileName));
            unreviewedLicenseFiles.remove(possibleFileToUpgrade);
            return;


        }

        for (String unusedFile : possibleUpgrades)
        {
            errorMsg.append("\t Check what license ").append(unusedFile).append(" is using and consider updating it.\n");
        }

    }

    private List<String> getPossibleFilesToUpgrade(final String filePrefix) {
        final List<String> possibleUpgrades = new ArrayList<String>();

        for (Map.Entry<String, Long> unusedFile : unreviewedLicenseFiles.entrySet())
        {
            if (retrievePrefixFromFileName(unusedFile.getKey()).equals(filePrefix))
            {
                possibleUpgrades.add(unusedFile.getKey());

            }
        }
        return possibleUpgrades;
    }


    private String retrievePrefixFromFileName(final String fileName)
    {
        final int endIndex = fileName.lastIndexOf(SEPARATOR);
        return endIndex > 0? fileName.substring(0, endIndex) : fileName;
    }

    //The rules for the file names can be found here: https://extranet.atlassian.com/x/S52Keg
    private String retrieveFilename(final String componentName, final BomEntry bomEntry)
    {

        if (bomEntry.isMaven())
        {
            return bomEntry.getGroup() + SEPARATOR + bomEntry.getArtifactId()
                    + SEPARATOR + bomEntry.getVersion() + ".txt";
        }
        else
        {
            return (componentName + SEPARATOR  + bomEntry.getVersion() + ".txt").replaceAll(FILEPATH_FORBIDDEN_CHARS, "");
        }

    }

}
