package org.codehaus.mojo.license;


import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.manager.WagonManager;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.MavenProjectBuilder;
import org.apache.maven.project.MavenProjectHelper;
import org.codehaus.mojo.license.api.DownloadArtifactException;
import org.codehaus.mojo.license.api.ExternalRepositoryHandler;
import org.codehaus.mojo.license.api.UploadArtifactException;
import org.codehaus.mojo.license.model.BomEntry;
import org.codehaus.mojo.license.model.Scope;
import org.codehaus.mojo.license.utils.ArtifactCreator;
import org.codehaus.mojo.license.utils.FileUtil;
import org.codehaus.plexus.util.FileUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This goal reads the BOM, extract dependencies
 * of certain licenses and check if you can find the sources
 * for them.
 *
 * The sources can be found in maven (usual repositories)
 * or in a special license repository.
 *
 * You can configure the goal to deploy the sources
 * or create a zip and attach to your build.
 *
 *
 * It assumes the BOM is correct.
 */
@Mojo( name = "open-source-distro", requiresProject = true, aggregator = true,
        requiresDependencyResolution = ResolutionScope.NONE, threadSafe = false)
public class OpenSourceDistroMojo extends AbstractBomExtractorMojo
{

    /**
     * Relevant licenses to filter from the BOM
     */
    final private static ImmutableList<String> FILTERED_LICENSES = ImmutableList.of(
            "GNU Lesser General Public License",
            "Common Development and Distribution License",
            "Common Public License",
            "Eclipse Public License",
            "Common Public License",
            "Mozilla Public License",
            "Academic Free License",
            "Artistic License",
            "Common Public License",
            "LaTeX Project Public License",
            "Open Software License");

    /**
     * maven classifier for sources
     */
    final private static String SOURCES_CLASSIFIER ="sources";

    /**
     * maven packaging type for sources
     */
    final private static String SOURCES_PACKAGING_TYPE ="jar";


    /**
     * Where to point the user to look for more information
     */
    private static final String DOCUMENTATION_LINK =
            "https://extranet.atlassian.com/questions/2171379387/what-to-do-if-the-open-source-distro-build-goes-red-";


    /**
     * The external repository where to look for sources
     * and where to deploy sources
     */
    @Parameter( property = "license.repositoryUrl",
            defaultValue = "https://maven.atlassian.com/content/repositories/atlassian-license/",
            required = true )
    private String repositoryUrl;

    /**
     * Deploy sources if they are not in the external license repo
     */
    @Parameter( property = "license.deploySources", defaultValue = "false", required = true )
    private boolean deploySources;


    /**
     * the version to use when deploying the sources
     * default is the version of the current project
     */
    @Parameter( property = "license.projectVersion", defaultValue = "${project.version}", required = true )
    private String projectVersion;

    /**
     * maven server ID to use to deploy
     * (check your $HOME/.m2/settings.xml if you want to change)
     */
    @Parameter( property = "license.repositoryId", defaultValue = "atlassian-private", required = true )
    private String repositoryId;


    /**
     * If it should create a zip file with all the sources
     */
    @Parameter( property = "license.createZipFile", defaultValue = "false", required = true )
    private boolean createZipFile;

    /**
     * The name of the zipfile ($name-VERSION.zip)
     * and the name of the tmp folder that holds the downloaded sources
     */
    @Parameter( property = "license.outputFileName", defaultValue = "open-source-distro", required = true )
    private String outputFileName;


    /**
     * Where the output files will be created
     */
    @Parameter(property = "license.targetDirectory", defaultValue = "${project.build.directory}")
    private String targetDirectory;


    @Component
    private MavenProjectBuilder mavenProjectBuilder;

    @Component
    private WagonManager wagonManager;

    @Component
    private MavenProjectHelper projectHelper;

    @Component
    private ExternalRepositoryHandler externalRepositoryHandler;

    @Component
    private ArtifactCreator artifactCreator;

    private FileUtil fileUtils = new FileUtil();


    final private List<BomEntry> errors = new ArrayList<BomEntry>();
    final private List<BomEntry> success = new ArrayList<BomEntry>();

    private File sourcesDirectory;



    @Override
    protected void init() throws Exception
    {
        super.init();

        if (!new File(targetDirectory).exists())
        {
            new File(targetDirectory).mkdirs();
        }

        if (createZipFile || deploySources)
        {
            sourcesDirectory = new File(targetDirectory, outputFileName);
            sourcesDirectory.mkdir();
            FileUtils.cleanDirectory(sourcesDirectory);
        }

    }

    @Override
    protected void doAction() throws Exception
    {
        final List<BomEntry> bomEntries = bomExtractor.retrieveBomEntries(thirdPartyFile, Scope.BINARY, true,
                FILTERED_LICENSES, true);

        final Map<BomEntry, File> filesToDeploy = new HashMap<BomEntry, File>();

        for (BomEntry bomEntry : bomEntries)
        {
            getLog().info("");
            getLog().info("Investigating sources (" + bomEntry.getComposedIdentifier() + ")");
            final boolean isArtifactPresentInExternalRepository = isArtifactPresentInExternalRepository(bomEntry);

            if ( isArtifactPresentInExternalRepository || isArtifactPresentInMaven(bomEntry) )
            {
                if (createZipFile
                        || (deploySources && !isArtifactPresentInExternalRepository))
                {
                    try
                    {
                        File destination = downloadSources(bomEntry, isArtifactPresentInExternalRepository);

                        if (deploySources)
                        {
                            filesToDeploy.put(bomEntry, destination);
                        }
                    }
                    catch (DownloadArtifactException e)
                    {
                        getLog().error("Exception when looking for sources: " + bomEntry.getComposedIdentifier());
                        getLog().error(e.getMessage() + ": " + e.getCause().getMessage());
                        getLog().debug(ExceptionUtils.getStackTrace(e));
                        errors.add(bomEntry);
                        continue;
                    }
                }
                success.add(bomEntry);
            }
            else
            {
                getLog().error("Error looking for sources: " + bomEntry.getComposedIdentifier() );
                errors.add(bomEntry);
            }

        }

        final File successFile = createOutputFile(success, "resolved-sources.txt");
        createOutputFile(errors, "missing-sources.txt");

        if (!errors.isEmpty())
        {
            getLog().error(getOutputMessage(errors, "The following artifacts don't have sources available"));

            getLog().error("\n\nCheck " + DOCUMENTATION_LINK + " for more information. \n\n");

            throw new IllegalArgumentException("It wasn't possible to find the sources of all required dependencies. See message above for details. ");
        }

        if (createZipFile)
        {

            File zipFile = new File(targetDirectory, outputFileName + "-"+ getProject().getVersion() + ".zip");
            getLog().info("Creating zip file " + zipFile.getAbsolutePath());
            fileUtils.zipDirectory(sourcesDirectory, zipFile);

            getLog().info("Attaching artifact " + zipFile.getAbsolutePath());
            projectHelper.attachArtifact(getProject(), "zip", "sources", zipFile);

        }

        if (deploySources)
        {
            deployAllArtifacts(filesToDeploy, successFile);
        }

        getLog().info(getOutputMessage(success, "Sources found so far"));
    }




    private void deployAllArtifacts(final Map<BomEntry, File> filesToDeploy, final File successFile)
                                                                    throws UploadArtifactException
    {

        final MavenProject mavenProject = getProject();

        for ( Map.Entry<BomEntry, File> entry : filesToDeploy.entrySet())
        {
            final BomEntry bomEntry = entry.getKey();
            final File file = entry.getValue();

            getLog().info("Deploying sources " + bomEntry.getComposedIdentifier());
            externalRepositoryHandler.deployArtifact(bomEntry.getGroup(), bomEntry.getArtifactId(), bomEntry.getVersion(),
                    SOURCES_CLASSIFIER, SOURCES_PACKAGING_TYPE, file, repositoryId, repositoryUrl, mavenProject.getFile());

            getLog().info("Deployment finished " + file.getAbsolutePath());

        }


        getLog().info("Deploying project output " + successFile.getAbsolutePath());

        // We are using a stable repository on nexus
        final String version = projectVersion.replace("-SNAPSHOT", "");
        externalRepositoryHandler.deployArtifact(mavenProject.getGroupId(), mavenProject.getArtifactId(), version,
                "", "txt", successFile, repositoryId, repositoryUrl, mavenProject.getFile());
        getLog().info("Deployment finished " + successFile.getAbsolutePath());

    }





    private File downloadSources(final BomEntry bomEntry, final boolean isArtifactPresentInExternalRepository)
            throws DownloadArtifactException, IOException
    {
        if (isArtifactPresentInExternalRepository)
        {
            File sourcesExternalRepo = downloadSourcesFromExternalRepository(bomEntry);

            File destination = new File(sourcesDirectory, sourcesExternalRepo.getName());
            FileUtils.copyFile(sourcesExternalRepo, destination);

            return destination;
        }

        final Artifact mavenArtifact = downloadSourcesFromMaven(bomEntry);

        File sourcesJar = mavenArtifact.getFile();
        File destination = new File(sourcesDirectory, sourcesJar.getName());
        FileUtils.copyFile(sourcesJar, destination);

        return destination;

    }


    private boolean isArtifactPresentInExternalRepository(BomEntry bomEntry)
    {

        try
        {
            final boolean found  = externalRepositoryHandler.checkArtifact(bomEntry.getGroup(), bomEntry.getArtifactId(),
                    bomEntry.getVersion(), SOURCES_CLASSIFIER, SOURCES_PACKAGING_TYPE, repositoryId, repositoryUrl);

            if (found)
            {
                getLog().info("Found sources - external repo (" + bomEntry.getComposedIdentifier() + ")");
            }
            else
            {
                getLog().warn("Couldn't find sources - external repository (" + bomEntry.getComposedIdentifier() + ")");
            }
            return found;
        }
        catch (DownloadArtifactException e)
        {
            getLog().warn("Couldn't find sources - maven (" + bomEntry.getComposedIdentifier() + ")");
            getLog().debug(e.getMessage() + " - " + e.getCause().getMessage());
            getLog().debug(ExceptionUtils.getStackTrace(e));
            return false;
        }

    }

    private boolean isArtifactPresentInMaven(BomEntry bomEntry)
    {
        try
        {
            artifactCreator.checkArtifact(bomEntry.getGroup(), bomEntry.getArtifactId(),
                    bomEntry.getVersion(), SOURCES_CLASSIFIER, SOURCES_PACKAGING_TYPE, remoteRepositories, artifactRepository);

            getLog().info("Found sources - maven (" + bomEntry.getComposedIdentifier() + ")" );

            return true;
        }
        catch (DownloadArtifactException e)
        {
            getLog().warn("Couldn't find sources - maven (" + bomEntry.getComposedIdentifier() + ")" + e.getCause().getMessage());
            getLog().debug(e.getMessage() + " - " + e.getCause().getMessage());
            getLog().debug(ExceptionUtils.getStackTrace(e));
            return false;
        }
    }

    private Artifact downloadSourcesFromMaven(BomEntry bomEntry) throws DownloadArtifactException
    {
        return artifactCreator.resolveArtifact(bomEntry.getGroup(), bomEntry.getArtifactId(),
                bomEntry.getVersion(), SOURCES_CLASSIFIER, SOURCES_PACKAGING_TYPE, remoteRepositories, artifactRepository);
    }

    private File downloadSourcesFromExternalRepository(BomEntry bomEntry) throws DownloadArtifactException
    {
        return externalRepositoryHandler.downloadArtifact(bomEntry.getGroup(), bomEntry.getArtifactId(),
                bomEntry.getVersion(), SOURCES_CLASSIFIER, SOURCES_PACKAGING_TYPE, sourcesDirectory, repositoryId, repositoryUrl);
    }


    private String getOutputMessage(final List<BomEntry> bomEntries, final String header)
    {
        final StringBuilder message = new StringBuilder("\n\n\n");
        message.append(header).append(" (");

        message.append(bomEntries.size()).append(" dependencies):\n");

        for (BomEntry bomEntry : bomEntries)
        {
            message.append("\t - ").append(bomEntry.toString()).append("\n");
        }

        return message.toString();
    }

    private File createOutputFile(final List<BomEntry> bomEntries, final String filename) throws IOException
    {
        final File outputFile = new File(targetDirectory, filename);

        if (!outputFile.exists())
        {
            outputFile.createNewFile();
        }

        final BufferedWriter buffer = new BufferedWriter(new FileWriter(outputFile));
        for (BomEntry bomEntry : bomEntries)
        {
            buffer.write(bomEntry.getComposedIdentifier());
            buffer.write(System.getProperty("line.separator"));
        }

        if (bomEntries.isEmpty())
        {
            //making sure the file is not empty, otherwise we can't deploy it
            buffer.write("No relevant dependencies found.");
            buffer.write(System.getProperty("line.separator"));
        }
        buffer.close();

        return outputFile;
    }

}
