package org.codehaus.mojo.license.api;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.codehaus.mojo.license.model.CandidateArtifact;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentMap;


public class ArtifactResolutionTask implements Callable<List<Artifact>>
{
    private final CandidateArtifact candidateArtifact;
    private final List<ArtifactRepository> remoteRepositories;
    private final ArtifactRepository localRepository;
    private final ConcurrentMap<ArtifactResolutionRequest, List<Artifact>> resolutionCache;
    private final boolean verbose;

    public ArtifactResolutionTask(final CandidateArtifact candidateArtifact,
                                  final List<ArtifactRepository> remoteRepositories,
                                  final ArtifactRepository localRepository,
                                  final ConcurrentMap<ArtifactResolutionRequest, List<Artifact>> resolutionCache,
                                  final boolean verbose)
    {
        this.candidateArtifact = candidateArtifact;
        this.remoteRepositories = remoteRepositories;
        this.localRepository = localRepository;
        this.resolutionCache = resolutionCache;
        this.verbose = verbose;
    }

    @Override
    public List<Artifact> call() throws Exception
    {
        return resolutionCache.get(new ArtifactResolutionRequest(candidateArtifact.getSha(), remoteRepositories, localRepository,
                candidateArtifact, verbose));
    }
}
