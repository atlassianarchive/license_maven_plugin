package org.codehaus.mojo.license.api;


import org.apache.maven.artifact.Artifact;
import org.codehaus.mojo.license.model.BomEntry;

import java.io.File;
import java.io.IOException;

public interface ExternalRepositoryHandler
{
    /**
     * Deploy artifact to external repo
     *
     * @param groupId
     * @param artifactId
     * @param version
     * @param classifier
     * @param packaging
     * @param file
     * @param repositoryId
     * @param repositoryUrl
     * @throws Exception
     */
    void deployArtifact(final String groupId, final String artifactId, final String version,
                               final String classifier, final String packaging, final File file,
                               final String repositoryId,
                               final String repositoryUrl, final File pomFile) throws UploadArtifactException;

    /**
     * Check if artifact exists on the repo
     *
     *
     * @param groupId
     * @param artifactId
     * @param version
     * @param classifier
     * @param packaging
     * @param repositoryId
     * @param repositoryUrl
     * @return
     * @throws Exception
     */
    boolean checkArtifact(final String groupId, final String artifactId, final String version,
                                 final String classifier, final String packaging,
                                 final String repositoryId,
                                 final String repositoryUrl) throws DownloadArtifactException;

    /**
     * Downloads the artifact if it exists.
     *
     * @param groupId
     * @param artifactId
     * @param version
     * @param classifier
     * @param packaging
     * @param outputDirectory
     * @param repositoryId
     * @param repositoryUrl
     * @return
     * @throws Exception
     */
    File downloadArtifact(final String groupId, final String artifactId, final String version,
                                 final String classifier, final String packaging, File outputDirectory,
                                 final String repositoryId,
                                 final String repositoryUrl) throws DownloadArtifactException;

}
