package org.codehaus.mojo.license.api;

import de.schlichtherle.truezip.file.TFile;
import de.schlichtherle.truezip.file.TFileInputStream;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.maven.artifact.Artifact;
import org.codehaus.mojo.license.model.CandidateArtifact;
import org.codehaus.mojo.license.utils.ArtifactCreator;
import org.codehaus.plexus.logging.Logger;
import org.codehaus.plexus.util.IOUtil;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.Callable;

class NestedArchiveSearchTask implements Callable<NestedArchiveSearchResult>
{

    private final String artifactId;
    private final TFile tFile;
    private final Map<String, CandidateArtifact> fileToArtifact;
    private final ArtifactCreator artifactCreator;
    private final Logger logger;

    public NestedArchiveSearchTask(final String artifactId, final TFile tFile,
                                   final Map<String, CandidateArtifact> fileToArtifact,
                                   final ArtifactCreator artifactCreator, final Logger logger)
    {
        this.artifactId = artifactId;
        this.tFile = tFile;
        this.fileToArtifact = fileToArtifact;
        this.artifactCreator = artifactCreator;
        this.logger = logger;
    }

    @Override
    public NestedArchiveSearchResult call() throws Exception
    {
        final String absolutePath = tFile.getAbsolutePath();
        logger.debug(getClass().getName() + " - Searching for nested artifacts in " + absolutePath);
        collectNestedArtifactsRecursively(this.tFile);
        logger.debug(getClass().getName() + " - For " + absolutePath + ", found " + fileToArtifact);
        return new NestedArchiveSearchResult(absolutePath, fileToArtifact);
    }

    private void collectNestedArtifactsRecursively(final TFile tFile)
    {
        TFile[] filesInArchive = tFile.listFiles(PossibleNestedArtifactFileFilter.INSTANCE);
        if (filesInArchive != null)
        {
            for (TFile fileInArchive : filesInArchive)
            {
                final String fileName = fileInArchive.getName();
                if (isArchive(fileName))
                {
                    logger.debug(artifactId + " contains " + fileInArchive.getAbsolutePath());
                    final String sha = getShaForFile(fileInArchive, fileName);
                    logger.debug(fileInArchive.getAbsolutePath() + " sha " + sha);
                    fileToArtifact.put(fileName, new CandidateArtifact(sha, fileName, fileInArchive.getAbsolutePath()));
                    collectNestedArtifactsRecursively(fileInArchive);
                }
                else if (isMetadataPomFile(fileInArchive))
                {
                    TFileInputStream tFileInputStream = null;
                    try
                    {
                        tFileInputStream = new TFileInputStream(fileInArchive.toNonArchiveFile());
                        final String enclosingArchiveFileName = fileInArchive.getEnclArchive().getName();
                        final Artifact artifact = artifactCreator.fromPom(tFileInputStream, enclosingArchiveFileName);

                        final CandidateArtifact candidateArtifact = fileToArtifact.get(enclosingArchiveFileName);
                        if (candidateArtifact != null)
                        {
                            candidateArtifact.addContainedArtifact(artifact);
                        }
                    }
                    catch (Exception e)
                    {
                        logger.error("An error occurred when trying to extract metadata from " + fileInArchive, e);
                    }
                    finally
                    {
                        IOUtil.close(tFileInputStream);
                    }
                }
                else if (fileInArchive.isDirectory())
                {
                    collectNestedArtifactsRecursively(fileInArchive);
                }
            }
        }
    }

    private String getShaForFile(final TFile archiveFile, final String fileName)
    {
        TFileInputStream tFileInputStream = null;
        try
        {
            tFileInputStream = new TFileInputStream(archiveFile.toNonArchiveFile());
            return DigestUtils.shaHex(new BufferedInputStream(tFileInputStream));
        }
        catch (IOException e)
        {
            logger.error("An error occurred generating the sha for " + fileName, e);
        }
        finally
        {
            IOUtil.close(tFileInputStream);
        }
        return null;
    }

    private static boolean isMetadataPomFile(final File archiveFile)
    {
        return archiveFile.getName().equals("pom.properties") && archiveFile.getAbsolutePath().contains("META-INF");
    }

    private static boolean isArchive(String fileName)
    {
        return fileName.endsWith(".jar") || fileName.endsWith(".zip");
    }

    private static class PossibleNestedArtifactFileFilter implements FileFilter
    {
        public static PossibleNestedArtifactFileFilter INSTANCE = new PossibleNestedArtifactFileFilter();

        @Override
        public boolean accept(File pathname)
        {
            final String fileName = pathname.getName();
            return (pathname.isDirectory() || isArchive(fileName) || isMetadataPomFile(pathname));
        }
    }
}
