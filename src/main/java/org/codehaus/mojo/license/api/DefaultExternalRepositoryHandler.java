package org.codehaus.mojo.license.api;


import org.apache.maven.artifact.manager.WagonManager;
import org.apache.maven.shared.invoker.DefaultInvocationRequest;
import org.apache.maven.shared.invoker.DefaultInvoker;
import org.apache.maven.shared.invoker.InvocationRequest;
import org.apache.maven.shared.invoker.InvocationResult;
import org.apache.maven.wagon.providers.http.HttpWagon;
import org.apache.maven.wagon.repository.Repository;
import org.codehaus.mojo.license.utils.ArtifactCreator;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.Properties;

/**
 * Default implementation of the {@link ExternalRepositoryHandler}.
 *
 * @plexus.component role="org.codehaus.mojo.license.api.ExternalRepositoryHandler" role-hint="default"
 * @since 1.3-atlassian-1.18
 */
public class DefaultExternalRepositoryHandler implements ExternalRepositoryHandler
{

    /**
     * @plexus.requirement
     */
    private ArtifactCreator artifactCreator;


    /**
     * @plexus.requirement
     */
    private WagonManager wagonManager;




    @Override
    public void deployArtifact(final String groupId, final String artifactId, final String version,
                               final String classifier, final String packaging, final File file, final String repositoryId,
                               final String repositoryUrl, final File pomFile) throws UploadArtifactException {

        try
        {
            final InvocationRequest request = new DefaultInvocationRequest();
            request.setGoals(Arrays.asList("deploy:deploy-file") );
            request.setPomFile(pomFile);
            final Properties arguments = new Properties();
            request.setProperties(arguments);
            arguments.put("groupId", groupId);
            arguments.put("artifactId", artifactId);
            arguments.put("version", version);
            arguments.put("classifier", classifier);
            arguments.put("packaging", packaging);
            arguments.put("generate-pom", "true");
            arguments.put("file", file.getAbsolutePath());
            arguments.put("url", repositoryUrl);
            arguments.put("repositoryId", repositoryId);



            final InvocationResult result = new DefaultInvoker().execute(request);

            if ( result.getExitCode() != 0 )
            {
                throw new Exception(result.getExecutionException());
            }
        }
        catch (RuntimeException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new UploadArtifactException("Failed to deploy.", e);
        }


    }


    @Override
    public File downloadArtifact(final String groupId, final String artifactId, final String version,
                                                          final String classifier, final String packaging,
                                                          final File outputDirectory, final String repositoryId,
                                                          final String repositoryUrl) throws DownloadArtifactException {

        final String nexusPath = getNexusDirectoryPath(groupId, artifactId, version, classifier, packaging);

        try
        {
            final Repository repository = new Repository(repositoryId, repositoryUrl);

            final HttpWagon httpWagon = (HttpWagon) wagonManager.getWagon(repository);
            httpWagon.connect(repository);

            File outputFile = new File(outputDirectory, getNexusFileName(groupId, artifactId, version, classifier, packaging));
            final FileOutputStream fos = new FileOutputStream(outputFile);
            httpWagon.getToStream(nexusPath, fos);
            fos.close();

            return outputFile;
        }
        catch (RuntimeException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new DownloadArtifactException("Unable to download artifact (path " + repositoryUrl + nexusPath + ")", e);
        }
    }

    @Override
    public boolean checkArtifact(final String groupId, final String artifactId, final String version,
                                 final String classifier, final String packaging, final String repositoryId,
                                 final String repositoryUrl) throws DownloadArtifactException
    {
        final Repository repository = new Repository(repositoryId, repositoryUrl);
        final String nexusPath = getNexusDirectoryPath(groupId, artifactId, version, classifier, packaging);


        final HttpWagon httpWagon;
        try
        {
            httpWagon = (HttpWagon) wagonManager.getWagon(repository);
            httpWagon.connect(repository);


            return httpWagon.resourceExists(nexusPath);

        }
        catch (RuntimeException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new DownloadArtifactException("Unable to find artifact (path " + repositoryUrl + nexusPath + ")", e);
        }

    }


    private String getNexusDirectoryPath(final String groupId, final String artifactId, final String version,
                                         final String classifier, final String packaging)
    {
        return String.format("/%s/%s/%s/%s", groupId.replace(".", "/"), artifactId ,
                version, getNexusFileName(groupId, artifactId, version, classifier, packaging));
    }

    private String getNexusFileName(final String groupId, final String artifactId, final String version,
                                    final String classifier, final String packaging)
    {
        return String.format("%s-%s-%s.%s", artifactId,  version, classifier, packaging);
    }



}
