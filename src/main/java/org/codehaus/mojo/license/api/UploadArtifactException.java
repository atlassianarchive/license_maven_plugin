package org.codehaus.mojo.license.api;

import org.codehaus.plexus.util.cli.CommandLineException;


public class UploadArtifactException extends Exception
{
    public UploadArtifactException(String message, Exception e)
    {
        super(message, e);
    }
}
