package org.codehaus.mojo.license.api;


import org.apache.maven.artifact.repository.ArtifactRepository;
import org.codehaus.mojo.license.model.CandidateArtifact;

import java.util.List;

public class ArtifactResolutionRequest
{
    private String sha;
    private List<ArtifactRepository> remoteRepositories;
    private ArtifactRepository localRepository;
    private CandidateArtifact candidateArtifact;
    private boolean verbose;

    public ArtifactResolutionRequest(final String sha, final List<ArtifactRepository> remoteRepositories,
                                     final ArtifactRepository localRepository, final CandidateArtifact candidateArtifact, boolean verbose)
    {
        this.sha = sha;
        this.remoteRepositories = remoteRepositories;
        this.localRepository = localRepository;
        this.candidateArtifact = candidateArtifact;
        this.verbose = verbose;
    }

    public String getSha()
    {
        return sha;
    }

    public List<ArtifactRepository> getRemoteRepositories()
    {
        return remoteRepositories;
    }

    public ArtifactRepository getLocalRepository()
    {
        return localRepository;
    }

    public CandidateArtifact getCandidateArtifact()
    {
        return candidateArtifact;
    }

    public boolean getVerbose()
    {
        return verbose;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArtifactResolutionRequest that = (ArtifactResolutionRequest) o;

        if (candidateArtifact != null ? !candidateArtifact.equals(that.candidateArtifact) : that.candidateArtifact != null)
            return false;
        if (sha != null ? !sha.equals(that.sha) : that.sha != null) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = sha != null ? sha.hashCode() : 0;
        result = 31 * result + (candidateArtifact != null ? candidateArtifact.hashCode() : 0);
        return result;
    }
}
