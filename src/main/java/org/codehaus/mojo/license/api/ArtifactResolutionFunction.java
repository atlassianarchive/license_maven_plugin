package org.codehaus.mojo.license.api;

import com.google.common.base.Function;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.AbstractArtifactResolutionException;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.codehaus.mojo.license.model.CandidateArtifact;
import org.codehaus.mojo.license.utils.ArtifactCreator;
import org.codehaus.plexus.logging.Logger;

import java.util.Collections;
import java.util.List;

class ArtifactResolutionFunction implements Function<ArtifactResolutionRequest, List<Artifact>>
{
    private final ArtifactResolver resolver;
    private final ArtifactCreator artifactCreator;
    private final Logger logger;

    public ArtifactResolutionFunction(final ArtifactResolver resolver, final ArtifactCreator artifactCreator,
                                      final Logger logger)
    {
        this.resolver = resolver;
        this.artifactCreator = artifactCreator;
        this.logger = logger;
    }

    public List<Artifact> apply(final ArtifactResolutionRequest artifactResolutionRequest)
    {
        final String candidateArtifactSha = artifactResolutionRequest.getSha();
        final List<ArtifactRepository> remoteRepositories = artifactResolutionRequest.getRemoteRepositories();
        final ArtifactRepository localRepository = artifactResolutionRequest.getLocalRepository();
        final CandidateArtifact candidateArtifact = artifactResolutionRequest.getCandidateArtifact();
        final boolean verbose = artifactResolutionRequest.getVerbose();

        if (!candidateArtifact.getContainedArtifacts().isEmpty())
        {
            final List<Artifact> containedArtifacts = candidateArtifact.getContainedArtifacts();
            for (Artifact containedArtifact : containedArtifacts)
            {
                try
                {
                    resolver.resolve(containedArtifact, remoteRepositories, localRepository);

                    if (verbose)
                    {
                        logger.info("\tNested jar identified " + candidateArtifact.getFileName() + " as [" + containedArtifact + "] ");
                    }
                }
                catch (AbstractArtifactResolutionException e)
                {
                    if (verbose)
                    {
                        logger.warn("Unable to resolve " + containedArtifact + ", its pom.xml metadata " +
                                "may have been malformed.");
                    }
                }
            }
            return containedArtifacts;
        }
        else
        {
            Artifact artifact = artifactCreator.fromSha(candidateArtifactSha, candidateArtifact.getFileName());
            if (artifact != null)
            {
                try
                {
                    resolver.resolve(artifact, remoteRepositories, localRepository);

                    if (verbose)
                    {
                        logger.info("\tNested jar identified " + candidateArtifact.getFileName() + " as [" + artifact + "] ");
                    }

                }
                catch (Exception e)
                {
                    throw new RuntimeException(e);
                }
                return Collections.singletonList(artifact);
            }
            else
            {
                logger.warn("\tUnable to resolve nested artifact " + candidateArtifact.getFileName() + " originating from "
                        + candidateArtifact.getFullPath() +
                        ". You can ignore this message if the binary should not be in the BOM.");
            }
        }
        return Collections.emptyList();
    }
}
