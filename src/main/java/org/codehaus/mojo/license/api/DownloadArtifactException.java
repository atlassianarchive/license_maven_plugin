package org.codehaus.mojo.license.api;


public class DownloadArtifactException extends Exception
{
    public DownloadArtifactException(String message, Exception e)
    {
        super(message, e);
    }
}
