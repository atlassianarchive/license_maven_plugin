package org.codehaus.mojo.license;

/*
 * #%L
 * License Maven Plugin
 * %%
 * Copyright (C) 2008 - 2011 CodeLutin, Codehaus, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.collections.CollectionUtils;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.ProjectBuildingException;
import org.codehaus.mojo.license.api.DefaultThirdPartyHelper;
import org.codehaus.mojo.license.api.DependenciesTool;
import org.codehaus.mojo.license.api.MavenProjectDependenciesConfigurator;
import org.codehaus.mojo.license.api.ThirdPartyHelper;
import org.codehaus.mojo.license.api.ThirdPartyTool;
import org.codehaus.mojo.license.api.ThirdPartyToolException;
import org.codehaus.mojo.license.model.Dependency;
import org.codehaus.mojo.license.model.LicenseMap;
import org.codehaus.mojo.license.model.Scope;
import org.codehaus.mojo.license.utils.FileUtil;
import org.codehaus.mojo.license.utils.MojoHelper;
import org.codehaus.mojo.license.utils.SortedProperties;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;

/**
 * Abstract mojo for all third-party mojos.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class AbstractAddThirdPartyMojo
    extends AbstractLicenseMojo
        implements MavenProjectDependenciesConfigurator
{


    /**
     * A flag to use the missing licenses file to consolidate the THID-PARTY file.
     *
     * @since 1.0
     */
    @Parameter( property = "license.useMissingFile", defaultValue = "false" )
    private boolean useMissingFile;

    /**
     * The file where to fill the license for dependencies with unknwon license.
     *
     * @since 1.0
     */
    @Parameter( property = "license.missingFile", defaultValue = "src/license/THIRD-PARTY.properties" )
    private File missingFile;

    /**
     * To merge licenses in final file.
     * <p/>
     * Each entry represents a merge (first license is main license to keep), licenses are separated by {@code |}.
     * <p/>
     * Example :
     * <p/>
     * <pre>
     * &lt;licenseMerges&gt;
     * &lt;licenseMerge&gt;The Apache Software License|Version 2.0,Apache License, Version 2.0&lt;/licenseMerge&gt;
     * &lt;/licenseMerges&gt;
     * &lt;/pre&gt;
     *
     * @parameter
     * @since 1.0
     */
    @Parameter
    private List<String> licenseMerges;

    /**
     * To specify some licenses to include (separated by {@code |}).
     * <p/>
     * If this parameter is filled and a license is not in this {@code whitelist} then build will failed when property
     * {@link #failIfWarning} is setted on.
     *
     * @since 1.1
     */
    @Parameter( property = "license.includedLicenses", defaultValue = "" )
    private String includedLicenses;

    /**
     * To specify some licenses to exclude (separated by {@code |}).
     * <p/>
     * If a such license is found then build will failed when property
     * {@link #failIfWarning} is setted on.
     *
     * @since 1.1
     */
    @Parameter( property = "license.excludedLicenses", defaultValue = "" )
    private String excludedLicenses;

    /**
     * The path of the bundled third party file to produce when
     * {@link #generateBundle} is on.
     * <p/>
     * <b>Note:</b> This option is not available for {@code pom} module types.
     *
     * @since 1.0
     */
    @Parameter( property = "license.bundleThirdPartyPath",
                defaultValue = "META-INF/${project.artifactId}-THIRD-PARTY.txt" )
    private String bundleThirdPartyPath;

    /**
     * A flag to copy a bundled version of the third-party file. This is usefull
     * to avoid for a final application collision name of third party file.
     * <p/>
     * The file will be copied at the {@link #bundleThirdPartyPath} location.
     *
     * @since 1.0
     */
    @Parameter( property = "license.generateBundle", defaultValue = "false" )
    private boolean generateBundle;

    /**
     * To force generation of the third-party file even if every thing is up to date.
     *
     * @since 1.0
     */
    @Parameter( property = "license.force", defaultValue = "false" )
    private boolean force;

    /**
     * A flag to fail the build if at least one dependency was detected without a license.
     *
     * @since 1.0
     */
    @Parameter( property = "license.failIfWarning", defaultValue = "false" )
    private boolean failIfWarning;

    /**
     * Template used to build the third-party file.
     * <p/>
     * (This template use freemarker).
     *
     * @since 1.1
     */
    @Parameter( property = "license.fileTemplate", defaultValue = "/org/codehaus/mojo/license/third-party-file.ftl" )
    private String fileTemplate;


    /**
     * Output to CSV instead of template.
     * Allows you to use non maven dependencies.
     *
     * @since 1.3-atlassian
     */
    @Parameter( property = "license.exportToCSV", defaultValue = "true" )
    private boolean exportToCSV;


    /**
     *
     * Allows you to use non nonMavenDependenciesFile.
     *
     * @since 1.3-atlassian
     */
    @Parameter( property = "license.useNonMavenDependenciesFile", defaultValue = "true" )
    private boolean useNonMavenDependenciesFile;

    /**
     *
     * File to get the non maven dependencies from
     *
     * @since 1.3-atlassian
     */

    @Parameter( property = "license.nonMavenDependenciesFile", defaultValue = "src/license/NON-MAVEN.properties" )
    private File nonMavenDependenciesFile;

    /**
     * Local Repository.
     *
     * @since 1.0.0
     */
    @Parameter( property = "localRepository", required = true, readonly = true )
    private ArtifactRepository localRepository;

    /**
     * Remote repositories used for the project.
     *
     * @since 1.0.0
     */
    @Parameter( property = "project.remoteArtifactRepositories", required = true, readonly = true )
    private List remoteRepositories;

    /**
     *
     * Allows you to use non nonMavenDependenciesFile.
     *
     * @since 1.3-atlassian-1.4
     */
    @Parameter( property = "license.scope", defaultValue = "binary" )
    private String moduleScope;


    /**
     *
     * Allows you to exclude some GAVs.
     *
     * @since 1.3-atlassian-1.4
     */
    @Parameter( property = "license.excludedGAVs", defaultValue = "" )
    private String excludedGAVs;



    /**
     * Licenses to ignore; artifacts won't be included in the BOM or tested.
     * Separate them by comma
     *
     * @since 1.3-atlassian-1.6
     */
    @Parameter( property = "license.ignoredLicenses", defaultValue = "Atlassian 3.0 End User License Agreement" )
    private String ignoredLicenses;



    /**
     * To specify the group:artifact to ignore (including its transitive dependencies) (separated by {@code ,}).
     * <p/>
     * Only works for artifacts on the maven dependency:tree,
     * nested jars will appear anyway.
     *
     * @since 1.3-atlassian-1.8
     */
    @Parameter( property = "license.ignoredArtifactsTrees", defaultValue = "" )
    private String ignoredArtifactsTrees;


    // ----------------------------------------------------------------------
    // Mojo Parameters
    // ----------------------------------------------------------------------

    /**
     * Deploy the third party missing file in maven repository.
     *
     * @since 1.0
     */
    @Parameter( property = "license.deployMissingFile", defaultValue = "true" )
    private boolean deployMissingFile;

    /**
     * Load from repositories third party missing files.
     *
     * @since 1.0
     */
    @Parameter( property = "license.useRepositoryMissingFiles", defaultValue = "true" )
    private boolean useRepositoryMissingFiles;

    /**
     * To execute or not this mojo if project packaging is pom.
     * <p/>
     * <strong>Note:</strong> The default value is {@code false}.
     *
     * @since 1.1
     */
    @Parameter( property = "license.acceptPomPackaging", defaultValue = "false" )
    private boolean acceptPomPackaging;

    /**
     * A filter to exclude some scopes.
     *
     * @since 1.1
     */
    @Parameter( property = "license.excludedScopes", defaultValue = "system" )
    private String excludedScopes;

    /**
     * A filter to include only some scopes, if let empty then all scopes will be used (no filter).
     *
     * @since 1.1
     */
    @Parameter( property = "license.includedScopes", defaultValue = "" )
    private String includedScopes;

    /**
     * A filter to exclude some GroupIds
     *
     * @since 1.1
     */
    @Parameter( property = "license.excludedGroups", defaultValue = "" )
    private String excludedGroups;

    /**
     * A filter to include only some GroupIds
     *
     * @since 1.1
     */
    @Parameter( property = "license.includedGroups", defaultValue = "" )
    private String includedGroups;

    /**
     * A filter to exclude some ArtifactsIds
     *
     * @since 1.1
     */
    @Parameter( property = "license.excludedArtifacts", defaultValue = "" )
    private String excludedArtifacts;

    /**
     * A filter to include only some ArtifactsIds
     *
     * @since 1.1
     */
    @Parameter( property = "license.includedArtifacts", defaultValue = "" )
    private String includedArtifacts;

    /**
     * Include transitive dependencies when downloading license files.
     *
     * @since 1.1
     */
    @Parameter( defaultValue = "true" )
    private boolean includeTransitiveDependencies;


    // ----------------------------------------------------------------------
    // Plexus components
    // ----------------------------------------------------------------------

    /**
     * Third party tool.
     *
     * @readonly
     * @since 1.0
     */
    @Component
    private ThirdPartyTool thirdPartyTool;

    /**
     * Dependencies tool.
     *
     * @readonly
     * @since 1.1
     */
    @Component
    private DependenciesTool dependenciesTool;

    // ----------------------------------------------------------------------
    // Private fields
    // ----------------------------------------------------------------------

    /**
     * Third-party helper (high level tool with common code for mojo and report).
     */
    private ThirdPartyHelper helper;

    private SortedMap<String, Dependency> projectDependencies;

    protected LicenseMap licenseMap;

    private File thirdPartyFile;

    private SortedProperties overrideMappings;

    private boolean doGenerate;

    private boolean doGenerateBundle;

    //private Map<String, NonMavenDependency> nonMavenDependencies;

    // ----------------------------------------------------------------------
    // Abstract Methods
    // ----------------------------------------------------------------------

    /**
     * Loads the dependencies of the project (as {@link MavenProject}, indexed by their gav.
     *
     * @return the map of dependencies of the maven project indexed by their gav.
     */
    protected abstract SortedMap<String, Dependency> loadDependencies() throws Exception;

    /**
     * Creates the unsafe mapping (says dependencies with no license given by their pom).
     * <p/>
     * Can come from loaded missing file or from dependencies with no license at all.
     *
     * @return the map of usafe mapping indexed by their gav.
     * @throws ProjectBuildingException if could not create maven porject for some dependencies
     * @throws IOException              if could not load missing file
     * @throws ThirdPartyToolException  for third party tool error
     */
    protected abstract SortedProperties createAllOverrideMapping()
            throws Exception, IOException, ThirdPartyToolException;


    protected abstract List<File> getNonMavenDependenciesFiles();

    // ----------------------------------------------------------------------
    // AbstractLicenseMojo Implementaton
    // ----------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    protected void init()
        throws Exception
    {

        Log log = getLog();

        if ( log.isDebugEnabled() )
        {

            // always be verbose in debug mode
            setVerbose( true );
        }

        thirdPartyFile = new File( getOutputDirectory(), getThirdPartyFilename() );

        long buildTimestamp = getBuildTimestamp();

        if ( isVerbose() )
        {
            log.info( "Build start   at : " + buildTimestamp );
            log.info( "third-party file : " + thirdPartyFile.lastModified() );
        }

        doGenerate = isForce() || !thirdPartyFile.exists() || buildTimestamp > thirdPartyFile.lastModified();

        if ( generateBundle )
        {

            File bundleFile = FileUtil.getFile( getOutputDirectory(), bundleThirdPartyPath );

            if ( isVerbose() )
            {
                log.info( "bundle third-party file : " + bundleFile.lastModified() );
            }
            doGenerateBundle = isForce() || !bundleFile.exists() || buildTimestamp > bundleFile.lastModified();
        }
        else
        {

            // not generating bundled file
            doGenerateBundle = false;
        }



        projectDependencies = loadDependencies();



        List<String> excludedGAVsList;

        if (excludedGAVs == null)
        {
            excludedGAVsList = Collections.emptyList();
        }
        else
        {
            excludedGAVsList = Arrays.asList(excludedGAVs.split(","));
        }


        licenseMap = getHelper().createLicenseMap( projectDependencies, getEncoding(), getModuleScope(), excludedGAVsList, getProject());

        if (useNonMavenDependenciesFile)
        {
            List<File> nonMavenDependenciesFiles = getNonMavenDependenciesFiles();
            getHelper().loadNonMavenDependencies(nonMavenDependenciesFiles, getEncoding(), getProject(), licenseMap);
        }

        if ( isUseMissingFile() && isDoGenerate() )
        {
            overrideMappings = createAllOverrideMapping();
        }
        getHelper().mergeLicenses(licenseMerges, licenseMap);

        getHelper().pickMostPermissiveLicenses(licenseMap, getOverrideFile().getAbsolutePath());

    }




    // ----------------------------------------------------------------------
    // Public Methods
    // ----------------------------------------------------------------------


    public String getIgnoredLicenses()
    {
        return ignoredLicenses;
    }


    public boolean isFailIfWarning()
    {
        return failIfWarning;
    }

    public SortedMap<String, Dependency> getProjectDependencies()
    {
        return projectDependencies;
    }

    public LicenseMap getLicenseMap()
    {
        return licenseMap;
    }

    public boolean isUseMissingFile()
    {
        return useMissingFile;
    }

    public File getOverrideFile()
    {
        return missingFile;
    }

    public SortedProperties getOverrideMappings()
    {
        return overrideMappings;
    }

    public boolean isForce()
    {
        return force;
    }

    public boolean isDoGenerate()
    {
        return doGenerate;
    }

    public boolean isDoGenerateBundle()
    {
        return doGenerateBundle;
    }



    // ----------------------------------------------------------------------
    // Protected Methods
    // ----------------------------------------------------------------------

    protected ThirdPartyHelper getHelper()
    {
        if ( helper == null )
        {
            helper =
                new DefaultThirdPartyHelper( getProject(), getEncoding(), isVerbose(), dependenciesTool, thirdPartyTool,
                                             localRepository, remoteRepositories, getLog() );
        }
        return helper;
    }

    public List<String> getExcludedLicenses()
    {
        String[] split = excludedLicenses == null ? new String[0] : excludedLicenses.split( "\\s*\\|\\s*" );
        return Arrays.asList( split );
    }

    public List<String> getIncludedLicenses()
    {
        String[] split = includedLicenses == null ? new String[0] : includedLicenses.split( "\\s*\\|\\s*" );
        return Arrays.asList( split );
    }

    protected boolean checkForbiddenLicenses()
    {
        List<String> whiteLicenses = getIncludedLicenses();
        List<String> blackLicenses = getExcludedLicenses();
        Set<String> unsafeLicenses = new HashSet<String>();
        if ( CollectionUtils.isNotEmpty( blackLicenses ) )
        {
            Set<String> licenses = getLicenseMap().getAllLicenses();
            getLog().info( "License blacklist: " + blackLicenses );


            for ( String excludeLicense : blackLicenses )
            {
                if ( licenses.contains( excludeLicense ))
                {
                    //bad license found
                    unsafeLicenses.add( excludeLicense );
                }
            }
        }

        if ( CollectionUtils.isNotEmpty( whiteLicenses ) )
        {
            Set<String> licenses = getLicenseMap().getAllLicenses();
            getLog().info( "License whitelist: " + whiteLicenses );

            for ( String license : licenses )
            {
                if ( !whiteLicenses.contains( license ))
                {
                    //bad license found
                    unsafeLicenses.add( license );
                }
            }
        }

        boolean safe = CollectionUtils.isEmpty( unsafeLicenses );

        if ( !safe)
        {
            Log log = getLog();
            //log.warn( "There is " + unsafeLicenses.size() + " forbidden licenses used:" );
            for ( String unsafeLicense : unsafeLicenses )
            {

                SortedSet<Dependency> deps = getLicenseMap().getDependenciesByLicense(unsafeLicense);

                if (!deps.isEmpty())
                {
                    StringBuilder sb = new StringBuilder();
                    sb.append( "\n\nForbidden License \"" ).append( unsafeLicense ).append( "\" used by " ).append( deps.size() );
                    sb.append(getHelper().pluralise(deps.size(), " dependency", " dependencies"));
                    sb.append(":");
                    for ( Dependency dep : deps )
                    {
                        sb.append( "\n -  " ).append( dep.getArtifactName() );
                    }

                    sb.append(".\n\n\nYou cannot use the ");
                    sb.append(getHelper().pluralise(deps.size(), "library", "libraries"));
                    sb.append(" above. ");
                    sb.append("\nIf the identified license is not correct, override it in the file ");
                    sb.append(missingFile != null? missingFile.getAbsolutePath() : "");
                    sb.append(".\n\n");

                    log.error(sb.toString());
                }
            }
        }
        return safe;
    }

    protected void writeThirdPartyFile()
        throws IOException
    {

        if ( doGenerate )
        {
             thirdPartyTool.writeThirdPartyFile( licenseMap, thirdPartyFile, isVerbose(), getEncoding(),
                        fileTemplate, exportToCSV);
        }

        if ( doGenerateBundle )
        {

            thirdPartyTool.writeBundleThirdPartyFile( thirdPartyFile, getOutputDirectory(), bundleThirdPartyPath );
        }
    }


    public File getNonMavenDependenciesFile()
    {
        return nonMavenDependenciesFile;
    }

    public Scope getModuleScope()
    {
        return Scope.valueOfIgnoreCaseUnrestricted(moduleScope);
    }


    // ----------------------------------------------------------------------
    // MavenProjectDependenciesConfigurator Implementaton
    // ----------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    public String getExcludedGroups()
    {
        return excludedGroups;
    }

    /**
     * {@inheritDoc}
     */
    public String getIncludedGroups()
    {
        return includedGroups;
    }

    /**
     * {@inheritDoc}
     */
    public List<String> getExcludedScopes()
    {
        return MojoHelper.getParams(excludedScopes);
    }

    /**
     * {@inheritDoc}
     */
    public List<String> getIncludedScopes()
    {
        return MojoHelper.getParams( includedScopes );
    }

    /**
     * {@inheritDoc}
     */
    public String getExcludedArtifacts()
    {
        return excludedArtifacts;
    }

    /**
     * {@inheritDoc}
     */
    public String getIncludedArtifacts()
    {
        return includedArtifacts;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isIncludeTransitiveDependencies()
    {
        return includeTransitiveDependencies;
    }

    public boolean isDeployMissingFile()
    {
        return deployMissingFile;
    }

    public boolean isAcceptPomPackaging()
    {
        return acceptPomPackaging;
    }

    public boolean isUseRepositoryMissingFiles()
    {
        return useRepositoryMissingFiles;
    }

    public String getIgnoredArtifactsTrees()
    {
        return ignoredArtifactsTrees;
    }
}