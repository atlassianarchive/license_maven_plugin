file = new File(basedir, 'licenses/org.tuckey--urlrewritefilter--4.0.3.txt');
assert file.exists()
content = file.text
assert content.count("Copyright (c) 2005-2012, Paul Tuckey") == 1

file = new File(basedir, 'licenses/junit--junit--4.11.txt');
assert file.exists()
content = file.text
assert content.count("Copyright (c) 2000-2006, www.hamcrest.org") == 1
assert !content.contains("The original content of the file.")

file = new File(basedir, 'licenses/org.hamcrest--hamcrest-core--1.3.txt');
assert file.exists()
content = file.text
assert content.count("Copyright (c) 2000-2006, www.hamcrest.org") == 1

file = new File(basedir, 'licenses/org.hamcrest--hamcrest-library--1.3.txt');
assert file.exists()
content = file.text
assert content.equals("This should not be overridden.\n")