file = new File(basedir, 'licenses/org.tuckey--urlrewritefilter--4.0.3.txt');
assert file.exists()
content = file.text
assert content.count("Licensed under the BSD License. ") == 1


file = new File(basedir, 'licenses/nonmavendependency--1.4.txt');
assert file.exists()
assert file.text.equals("")


file = new File(basedir, 'licenses/org.codehaus.woodstox--wstx-asl--3.2.4.txt');
assert file.exists()
content = file.text
assert content.startsWith("This product currently only contains code developed by authors")     //Notice file
assert content.count("This copy of Woodstox XML processor") == 1  //License file


file = new File(basedir, 'licenses/org.codehaus.jackson--jackson-xc--1.9.2.txt');
assert file.exists()
content = file.text
assert content.startsWith("This product currently only contains code developed by authors")     //Notice file
assert content.count("This copy of Jackson JSON processor is licensed under the") == 1 //License file


file = new File(basedir, 'licenses/xml-security--xmlsec--1.4.2.txt');
assert file.exists()
content = file.text
assert content.startsWith("This product contains software developed by")     //Notice file
assert content.count("1. Definitions.")  == 1 //License file


file = new File(basedir, 'licenses/org.codehaus.jettison--jettison--1.1.txt');
assert file.exists()
content = file.text
assert content.count("1. Definitions.")  == 1 //License file


file = new File(basedir, 'licenses/org.springframework--spring-aop--3.2.3.RELEASE.txt');
assert file.exists()
content = file.text
assert content.startsWith("Spring Framework 3.2.3.RELEASE")     //Notice file
assert content.count("1. Definitions.")  == 1//License file

file = new File(basedir, 'licenses/org.apache.sshd--apache-sshd--0.7.0.txt');
assert !file.exists()
