/*
 * #%L
 * License Maven Plugin
 * %%
 * Copyright (C) 2008 - 2011 CodeLutin, Codehaus, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

file = new File(basedir, 'build.log');
content = file.text;

/* Check the "unused files" session
 *  Renamed files should not appear! */
assert content.contains("The following files are not necessary")
assert content.contains(" - org.hibernate--hibernate-core--4.1.8.Final.txt")
assert content.contains(" - org.hibernate--hibernate-core--4.1.9.Final.txt")
assert !content.contains(" - jquery-hashchange--1.3.txt")
assert !content.contains(" - library-with-empty-license-file--1.3.txt")




/** If it has more than one option to upgrade, do nothing other than showing them **/
assert content.contains("The following files are missing or empty in the license folder")
assert content.contains(" - org.hibernate--hibernate-core--4.1.10.Final.txt")
assert content.contains("Check what license org.hibernate--hibernate-core--4.1.9.Final.txt")
assert content.contains("Check what license org.hibernate--hibernate-core--4.1.8.Final.txt")
assert !new File(basedir, 'licenses/org.hibernate--hibernate-core--4.1.10.Final.txt').exists()
assert new File(basedir, 'licenses/org.hibernate--hibernate-core--4.1.9.Final.txt').exists()
assert new File(basedir, 'licenses/org.hibernate--hibernate-core--4.1.8.Final.txt').exists()


/* If only one option, rename it */
assert content.contains(" - jquery-hashchange--1.4.txt")
assert content.contains("Renaming the file jquery-hashchange--1.3.txt")
assert new File(basedir, 'licenses/jquery-hashchange--1.4.txt').exists()
assert !new File(basedir, 'licenses/jquery-hashchange--1.3.txt').exists()


/* even if the file is empty. We are ignoring it here. */
assert content.contains(" - library-with-empty-license-file--1.4.txt")
assert content.contains("Renaming the file library-with-empty-license-file--1.3.txt")
assert new File(basedir, 'licenses/library-with-empty-license-file--1.4.txt').exists()
assert !new File(basedir, 'licenses/library-with-empty-license-file--1.3.txt').exists()



/* If you can't find anything to upgrade, we just create an empty file */
assert content.contains(" - another-library--1.4.txt")
assert new File(basedir, 'licenses/another-library--1.4.txt').exists()
assert content.contains("Creating a file for it")


assert content.contains("Some license files are missing")

return true;

