file = new File(basedir, 'target/open-source-distro/only-internal-1.0-sources.jar');
assert file.exists()

file = new File(basedir, 'target/open-source-distro/only-external-repo-1.0-sources.jar');
assert file.exists()


file = new File(basedir, 'target/open-source-distro-1.0-SNAPSHOT.zip');
assert file.exists()

file = new File(basedir, 'build.log');
content = file.text

assert content.count("Uploaded: https://maven.atlassian.com/private-snapshot/org/codehaus/mojo/license/test/" +
        "deployment-zip-file/1.0-SNAPSHOT/deployment-zip-file-1.0-") == 2


return true;

