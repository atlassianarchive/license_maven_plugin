
file = new File(basedir, 'target/missing-sources.txt');
assert file.text.equals("No relevant dependencies found.\n")

file = new File(basedir, 'target/resolved-sources.txt');
assert file.text.equals('com.atlassian.image:atlassian-image-consumer:jar:1.0.1\n' +
                        'javax.ws.rs:jsr311-api:jar:1.1\n' +
                        'com.sun.jersey:jersey-client:bundle:1.8-atlassian-11\n')


return true;

