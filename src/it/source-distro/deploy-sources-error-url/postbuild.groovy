file = new File(basedir, 'target/open-source-distro-1.0-SNAPSHOT.zip');
assert !file.exists()

file = new File(basedir, 'build.log');
content = file.text
assert !content.contains("Uploaded: https://maven.atlassian.com/")
assert content.contains("Return code is: 405, ReasonPhrase: Method Not Allowed.")

return true;

