file = new File(basedir, 'build.log');
content = file.text

assert content.contains("Uploaded: https://maven.atlassian.com/content/repositories/atlassian-license-test/" +
        "org/codehaus/mojo/license/test/alternate-version-file/1.5/alternate-version-file-1.5.txt")

assert !content.contains("Uploading: https://maven.atlassian.com/content/repositories/atlassian-license-test/" +
        "org/codehaus/mojo/license/test/alternate-version-file/1.0/")


return true;

