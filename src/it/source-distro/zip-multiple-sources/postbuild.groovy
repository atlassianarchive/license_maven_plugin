file = new File(basedir, 'target/open-source-distro/')
assert file.list().length == 3


def originFiles = []


file = new File(basedir, 'target/open-source-distro/atlassian-image-consumer-1.0.1-sources.jar');
assert file.exists() && file.length() > 0
originFiles.add([name: 'atlassian-image-consumer-1.0.1-sources.jar', size: file.length()])


file = new File(basedir, 'target/open-source-distro/jersey-client-1.8-atlassian-11-sources.jar');
assert file.exists() && file.length() > 0
originFiles.add([name: 'jersey-client-1.8-atlassian-11-sources.jar', size: file.length()])


file = new File(basedir, 'target/open-source-distro/jsr311-api-1.1-sources.jar');
assert file.exists() && file.length() > 0
originFiles.add([name: 'jsr311-api-1.1-sources.jar', size: file.length()])

def zipFile = new File(basedir, 'target/open-source-distro-1.0-SNAPSHOT.zip')
assert zipFile.exists()

def generatedFiles = new java.util.zip.ZipFile(zipFile).entries().collect{ entry ->
    [name: entry.name, size: entry.size]
}

println generatedFiles

assert generatedFiles.sort() == originFiles.sort()



return true;

