//* this test requires another-only-internal to be deleted from atlassian-license

file = new File(basedir, 'target/open-source-distro-1.0-SNAPSHOT.zip');
assert !file.exists()

file = new File(basedir, 'build.log');
content = file.text

assert content.contains("Uploaded: https://maven.atlassian.com/content/repositories/atlassian-license-test/" +
        "org/codehaus/mojo/license/test/another-only-internal/1.0/another-only-internal-1.0-sources.jar")

assert !content.contains("https://maven.atlassian.com/content/repositories/atlassian-license-test/" +
        "org/codehaus/mojo/license/test/only-external-repo")

assert content.contains("Uploaded: https://maven.atlassian.com/content/repositories/atlassian-license-test/" +
        "org/codehaus/mojo/license/test/deploy-sources/1.0/deploy-sources-1.0.txt")

return true;

