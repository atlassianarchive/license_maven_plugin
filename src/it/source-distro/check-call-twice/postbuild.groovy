
file = new File(basedir, 'target/missing-sources.txt');
assert file.text.equals("No relevant dependencies found.\n")

file = new File(basedir, 'target/resolved-sources.txt');
assert file.readLines().size() == 1


return true;

