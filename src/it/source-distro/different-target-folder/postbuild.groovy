file = new File(basedir, 'target/open-source-distro')
assert !file.exists()


file = new File(basedir, 'target/other-directory/open-source-distro');
assert file.exists()


file = new File(basedir, 'target/other-directory/open-source-distro-1.0-SNAPSHOT.zip');
assert file.exists()


return true;

