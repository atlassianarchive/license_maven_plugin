assert !new File(basedir, 'target/open-source-distro-1.0.zip').exists();


file = new File(basedir, 'target/missing-sources.txt');
assert file.text.equals('com.atlassian.image:atlassian-consumer:jar:1.0.1\n' +
                        'alt:alt:jar:0.07-jdk1.3\n')

file = new File(basedir, 'target/resolved-sources.txt');
assert file.text.equals("No relevant dependencies found.\n")


file = new File(basedir, 'build.log');
assert !file.text.contains("Uploaded: https://maven.atlassian.com/")


return true;

