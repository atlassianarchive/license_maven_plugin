file = new File(basedir, 'build.log');
content = file.text

assert content.contains("Uploaded: https://maven.atlassian.com/content/repositories/atlassian-license-test/" +
        "org/codehaus/mojo/license/test/alternate-valid-pom-file/1.0/alternate-valid-pom-file-1.0.txt")

assert !content.contains("Uploading: https://maven.atlassian.com/content/repositories/atlassian-license-test/" +
        "org/codehaus/mojo/license/test/invalid-pom")


return true;

