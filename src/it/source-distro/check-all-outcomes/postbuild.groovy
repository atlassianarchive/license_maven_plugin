file = new File(basedir, 'target/missing-sources.txt');
assert file.text.equals('alt:alt:jar:0.07-jdk1.3\n')

file = new File(basedir, 'target/resolved-sources.txt');
assert file.text.equals('org.codehaus.mojo.license.test:only-internal:jar:1.0\n' +
                        'org.codehaus.mojo.license.test:only-external-repo:jar:1.0\n')

file = new File(basedir, 'target/open-source-distro');
assert !file.exists()


file = new File(basedir, 'build.log');
content = file.text

assert content.contains("The following artifacts don't have sources available")
assert content.contains("alt:alt:jar:0.07-jdk1.3")


return true;

