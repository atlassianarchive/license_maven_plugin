file = new File(basedir, 'target/open-source-distro-1.0-SNAPSHOT.zip');
assert !file.exists()

file = new File(basedir, 'build.log');
assert file.text.contains("Return code is: 401, ReasonPhrase: Unauthorized.")
assert !file.text.contains("Uploaded: https://maven.atlassian.com/")


return true;

